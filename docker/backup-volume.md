## Backup
```shell=
docker run -v some_volume:/volume -v /tmp:/backup --rm loomchild/volume-backup backup archive1
```

## Restore
```shell=
docker run -v some_volume:/volume -v /tmp:/backup --rm loomchild/volume-backup restore archive1
```

## Example
### Create volume
```shell=
docker volume create txt_data
```
### Test volume
```shell=
docker run -d -v txt_data:/world busybox ls /world
```

### Create data in volume
```shell=
docker run -d -v txt_data:/world busybox sh -c 'echo "test" > /world/test.txt'
docker run -i -v txt_data:/world busybox cat /world/test.txt
```

### Backup volume
```shell=
docker run -v txt_data:/volume -v /tmp:/backup --rm loomchild/volume-backup backup txt_volume
ls /tmp
# txt_volume.tar.bz2
```

### Create same volume name in new machine
```shell=
# new machine
docker volume create txt_data
```

### Retrieve backup data from old machine to new machine
```shell=
# new machine
scp user@old.machine:/tmp/txt_volume.tar.bz2 .
```

### Restore volume
```shell=
docker run -v txt_data:/volume -v $PWD:/backup --rm loomchild/volume-backup restore txt_volume
```