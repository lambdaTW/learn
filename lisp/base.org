* symbol
  - special symbol
    * '|HELLO WOLD|
      - |HELLO WOLD|
  - string to symbol
    * (intern "name")
      - |name|
  - string to keyword symbol
    * (intern "name" :keyword)
      - :|name|
* keyword
  - make key
    * keyword:|hello|
      - :|hello|
  - function
    - see ./lib/to-key

* vector
  - bit
    * #*101010
      - #*101010

