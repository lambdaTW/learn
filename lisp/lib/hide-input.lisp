;user@shell$ sbcl --noinform --load hide-input.lisp
;* (sb-ext:save-lisp-and-die "hello.exe" :toplevel #'main :executable t)

(defun echo-off ()                                                     
   (let ((tm (sb-posix:tcgetattr sb-sys:*tty*)))                        
     (setf (sb-posix:termios-lflag tm)                                  
 	  (logandc2 (sb-posix:termios-lflag tm) sb-posix:echo))        
     (sb-posix:tcsetattr sb-sys:*tty* sb-posix:tcsanow tm)))            
                                                                        
 (defun echo-on ()                                                      
   (let ((tm (sb-posix:tcgetattr sb-sys:*tty*)))                        
     (setf (sb-posix:termios-lflag tm)                                  
 	  (logior (sb-posix:termios-lflag tm) sb-posix:echo))          
     (sb-posix:tcsetattr sb-sys:*tty* sb-posix:tcsanow tm)))

(defun main ()
  (let ((s nil))
    (echo-off)
    (setf s (read))
    ))
