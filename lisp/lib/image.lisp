(defun make-image-file (imei file-name)
  (make-pathname
   :directory (list :absolute "tmp" imei)
   :name file-name
   :type "imge"))

(defun store-image (file data)
  (with-open-file (str file
		     :direction :output
		     :if-exists :supersede
		     :if-does-not-exist :create)
    (format str data)))

(defun join-string-list (string-list)
  (format nil "~{~A~%~}" string-list))

(defun get-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
       while line
	           collect line)))

(defroute ("/upload" :method :POST) (&key _parsed)
  (let* ((image (cdr (assoc "imageString" _parsed :test #'string=)))
	 (imei (write-to-string (gethash :login *session*)))
	 (file (make-image-file imei "test"))
	 (path file))
    (ensure-directories-exist (directory-namestring path))
    (store-image file image)
    (render-json (list :|status| T))))

(defroute "/download" ()
  (join-string-list (get-file (make-image-file (write-to-string (gethash :login *session*)) "test"))))
