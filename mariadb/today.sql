USING DATE:

SELECT * FROM dates 
WHERE dte = cast(now() AS date)
;
USING DATETIME:

SELECT * FROM datetimes 
WHERE dtm >= cast((now()) AS date)
AND dtm < cast((now() + interval 1 DAY) AS date)
;
