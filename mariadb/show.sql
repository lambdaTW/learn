-- show all db
SHOW DATABASES;

-- show tables
SHOW TABLES;

-- show table schema
desc <tableName>;

-- show view
SHOW CREATE VIEW <viewName>;

-- show procedure
SHOW CREATE PROCEDURE <procedureName>;

-- show trigger
SHOW TRIGGERS FROM test WHERE `Table` = 'user' \G;

-- show foreign
SELECT
  TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
  REFERENCED_TABLE_NAME = '<table>';
