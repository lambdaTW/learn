* E11 (Python 工程師)
** 筆試題目(可用筆電)
1. 1+3+5...+23
2. html paser
3. how to store home info (address, gps, ...)

* 碁曄科技股份有限公司(Linux 研發工程師)
** 問答
1. docker 有別於其他 vm 的不同
   - 主管回答：隔離
2. docker 底層
   - 主管回答：cgroups
3. ci automatic
   - 因為聽不懂英文答不出來
4. 問相關經驗、秀專案

* StreetVoice(Sr. Python Web Developer)
** 問答
1. python immutable types
   - 沒遇過根本不會
2. decorator
   - 直接 show my code
3. classmethod & staticmethod diff
4. GIL
5. database transaction
   1. 壞掉後要 rollback(他想聽這個詞...我說了一堆資料庫老師說的設計方法 QWQ)
6. 用過哪些 “dunder”
   - __init__
   - __getitem__
   - __iter__
7. 使用者瀏覽 django web 資料流
   1. DNS
   2. nginx
      - 重點在 unix socket forward 還是 TCP forward
   3. wsgi
      - 重點在 middleware(ref:http://blog.ez2learn.com/2010/01/27/introduction-to-wsgi/)
   4. django middleware
   5. django urls
   6. django views

* 心得
很多專業術語主管念的可能聽不懂，下次一定要要求看到字，以消歧義
