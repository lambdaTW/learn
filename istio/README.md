## Overall
```
(Request) protocol://hosts/path:port ->
(Gateway: Ingress) select `protocol` and `port` (provide: SLL key handshake) ->
(Finally map to the `Virtual services` hsots) ->
(Virtual services) select `protocol`, `hosts`, `URL `URL path` and `HTTP headers` ->
(Finally map to the `Destination` subset)
(Destination) select `label` (provide pods load balance) ->
(Finally map to the k8s Service)
```

## Gateway
NOTICE: The request from the `mesh` to outside services are blocked by default
### Flow
#### Select objects
- Protocol (http, https, mutual https, tcp)
- Expose port
- Host (The ingress or egress domain)

#### Next responsible component
Virtual services

## Virtual services
### Power
#### `Fault Injection`
You can set `delay` time for the fault request
#### `Traffic Shifting`
You can set `weight` for every single destination
##### Example
Service A: 50
Service B: 50
#### `Request Timeouts`
You can let some service unavailable (503), if it out off the `timeout`
##### Example
```
Service A --call(set time out: 0.5s)--> Service B --call(set delay: 2s)--> Service C
```
Finally the Service A will get 503 error from Service B

### Flow
#### Select objects
- Protocol
- Host
- URL path
- HTTP headers
#### Next responsible component
Destination

## Destination
### Power
#### `Circuit Breaking`
You can limit the connection of connecting the backend, return `503` if the connection is filled
##### TCP
You can limit `maxConnections`, `connectTimeout` and `tcpKeepalive`
##### HTTP
You can limit `http1MaxPendingRequests`, `maxRequestsPerConnection`, `http2MaxRequests`, `maxRetries` and `idleTimeout`
### Flow
#### Select objects
- labels
#### Next responsible component
Kubernetes Service
