MVCC
===
https://github.com/TritonHo/slides/blob/master/Taipei%202015-01%20talk/Introduction_to_database.pdf

# Multiversion concurrency control
每筆資料會有不同的版本每個 transation 都會在自己的那個版本工作 (Snapshot Isolation)，所以讀取時不用管其他寫入，所以不需要因為讀取而鎖住寫入
- READ 永遠不會被 BLOCK
- READ 也永遠不會引起 BLOCK
- 不須讀取鎖，只需要寫入鎖
## Isolation
### Committed reads
- 所有 Query 只會考慮已經 committed 的最新版本
- 對資料 rows 進行 INSERT/UPDATE/DELETE 時，會加上 WRITE_LOCK 直到完成，LOCK 期間，其他要做 INSERT/UPDATE/DELETE 的 transation 都會被 BLOCK
- 正常情況下，配合 conflict materialization 和 conflict promotion，就可以應付大部分的 Isolation Read phenomena
### Repeatable reads
- 所有 Query 只會考慮已經 committed 且在此 transation 開始前已經存在的最新版本
- 在寫入時，額外檢查目標 rows 是否有在此 transation 建立後的新版本，如果有就 raise exception 並且 rollback
### Serializable
- 每個 Query 的 Predicate 加上 Predicate monitoring
- 當有新版本的 committed rows 滿足 Predicate，而且他的版本時間比本次 transation 還要新的話就 raise exception 並且 rollback
- 高 CPU 要求，不建議使用

## 優點
 - 沒有 READ_LOCK，且 READ 也不用檢查 WRITE_LOCK，系統中 LOCK 總數大量減少
   - LOCK Manager 工作量大量減少
   - Deadlock detection 變簡單，也工作量大減
 - 先天讓 Deadlock 大幅減少
 - Blocking 時間大大減少
 - 高流量時，性能比傳統的 Isolation 更好
## 缺點
 - 多個版本的資料同時生存於資料庫中，所以資料庫要管理舊版的生命週期
 - 每個 transation 比傳統的 Isolation 用了更多的 CPU 和 Disk IO

## 補充
 - MySQL 5.5 支援 MVCC
 - PostgreSQL 支援 MVCC