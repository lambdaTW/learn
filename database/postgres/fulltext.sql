SELECT id, ts_rank_cd(array[1.0, 0.5, 0.5, 0.5],
                      setweight(to_tsvector(title), 'D') ||
                      setweight(to_tsvector(tags::json), 'C') ||
                      setweight(to_tsvector(content), 'B') ||
                      setweight(to_tsvector(description), 'A'),
                      query,
                      32 /* rank/(rank+1) */ ) AS rank
FROM contents_content, to_tsquery(%s) query
WHERE  query @@ to_tsvector(title) OR
       query @@ to_tsvector(tags) OR
       query @@ to_tsvector(content) OR
       query @@ to_tsvector(description) OR
ORDER BY rank DESC
LIMIT 10
