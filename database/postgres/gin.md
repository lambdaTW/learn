## Install gin extension

```sql
CREATE EXTENSION IF NOT EXISTS pg_trgm;
```

## Set default operator class

```sql
UPDATE pg_opclass SET opcdefault = true WHERE opcname='gin_trgm_ops';
```

## Create index for table's column
```sql
CREATE INDEX <index_name>
    ON <schema_name>.<table_name> USING gin
    (<column_name>)
    TABLESPACE pg_default;
```