## Error from pk sequence not sync
`Error: duplicate key value violates unique constraint`

## Solution
Update the sequence next default value

```sql
SELECT setval('<table_name>_<column_name>_seq', max(<column_name>))
FROM <table_name>;
```

### Example
Table: content
PK column name: id

```sql
SELECT setval('content_id_seq', max(id))
FROM content;
```

## Get all sequence from DB
```sql
SELECT * FROM information_schema.sequences;
```
