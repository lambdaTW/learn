# MySQL fulltext search
 - limit
   - DB engine need use MyISAM
   - not support LONGTEXT field type
 - Can
   - search chinese be segmented

# Postgresql fulltext search
 - https://coderwall.com/p/mvsoyg/django-dumpdata-and-loaddata
 - http://www.racksam.com/2016/05/03/chinese-full-text-searching-with-postgresql-zhparser-and-rails/#more
 - http://rachbelaid.com/postgres-full-text-search-is-good-enough/
 - https://docs.djangoproject.com/en/2.1/topics/db/sql/#passing-parameters-into-raw