Database
===
# MySQL
## Data Block Size 16kB
較大的 Block Size 可以存放的 rows 會比較多
## 資料儲存結構
### Innodb Index-Organized Table
http://philipzhong.blogspot.com/2011/06/how-to-improve-mysql-select-count.html

InnoDB儲存表和 index 在一個空間中，此表空間可能包含幾個檔(有可能在不同disk partitions)，此點和MyISAM不同，MyISAM是一個表一個檔案，這差一點造成InnoDB表個可以非常大，即使是超過系統的file size (例如 2GB) 都可處理
使用依照 pk 排列的 B+tree 而且，資料被儲存在 B+tree 的葉子中
### MyISAM Heap Table
使用 MyISAM 將具備三種 檔案：
    - .frm (table format)
    - .MYD (data file)
    - .MYI (index file)
使用依照 pk 排列的 B+tree 而且，資料的硬碟位置被儲存在 B+tree 的葉子中，故拿到位址後會在產生一次 Disk IO 拿取真實資料
PS: 單表就一個檔案
## Index
### Innodb (cluster index)
#### PK index
依照 pk 排序儲存於 B+tree 中，葉節點存資料
#### Secondary key index
依照 index 的 field 排序儲存於 B+tree 中，葉節點存資料的 pk，故拿 pk 後會要在從 PK index 的 B+tree 拿資料
PS: 若無 PK 會先選 NOT NULL UNIQUE 的欄位，若以上都無則給每個資料列 6-byte 的 ROW_ID
### MyISAM (none cluster index)
依照 index 的 field 排序儲存於 B+tree 中，葉節點存資料的硬碟位置，故拿到位址後會在產生一次 Disk IO 拿取真實資料

# PostgreSQL
https://stackoverflow.com/questions/44299398/how-do-postgresql-indexes-reference-rows

## Data Block Size 8kB
較小的 Block Size 儲存的 rows 會比較少
故： 多個 TX 更動同一個 Block 內的 rows 可能性較小，較不容易產生 hot spot
再做 index leaf node splitting/merging 所影響的 rows 較少，但是要做比較多次，雖然每次的痛苦程度小於較大 Block Size 
## 資料儲存結構 Heap Table
使用依照 pk 排列的 B+tree 而且，資料的硬碟位置被儲存在 B+tree 的葉子中，故拿到位址後會在產生一次 Disk IO 拿取真實資料
