# Ways to prevent duplicate critical zone (payment) execute by multi-users
## Let the Transaction with `Repeatable Read` isolation level
### REF
 - [HTTP transactions](https://brandur.org/http-transactions)
## Use the `Idempotency Key`
### REF
 - [The design of Stripe's Idempotency Keys](https://blog.frost.tw/posts/2017/10/30/The-design-of-Stripe-s-Idempotency-Keys/)
# Ways to design robust and predictable APIs with idempotency
## Idempotency
[REF](https://stripe.com/blog/idempotency)
## Async job
[REF](https://brandur.org/job-drain#transactions-as-gates)
