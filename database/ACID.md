ACID
===
- https://github.com/TritonHo/slides/blob/master/Taipei%202015-01%20talk/Introduction_to_database.pdf
- https://github.com/TritonHo/slides/blob/master/Taipei%202015-08%20course/lesson0.pdf
# Atomicity
同一個 TX (transaction) 內的資料更動必須被全部執行 (committed) / 全部不被執行 (rollbacked)
亦即，保障資料庫數據安全的移到下一個正確狀態
# Consistency
所有數據更動都應該符合 data types, constrains, triggers, cascades，不符合規範的資料不應該被儲存（commit），以保障資料的正確性
亦即，保證系統在移動到下一個階段失敗時，能安全的回到本來的正確狀態
# Isolation
同時執行的 transaction 必須維持其獨立性
資料最終的執行結果必須是某一執行順序的結果
亦即，不能發生 Race condition (競爭條件：電腦嘗試覆蓋相同或者舊的資料，而此時舊的資料仍在被讀取。結果可能是下面的一個或者多個情況：機器當機、出現非法操作並結束程式、錯誤的讀取舊資料、或者錯誤的寫入新資料。)

## Isolation Read phenomena
https://zh.wikipedia.org/wiki/%E4%BA%8B%E5%8B%99%E9%9A%94%E9%9B%A2
https://en.wikipedia.org/wiki/Isolation_(database_systems)
### Dirty reads
一個 transation 讀取到另一個還沒有 commit 的資料，要是此修改被 rollback 或是再度被修改那讀出來的資料就不正確了

```SQL
/* Transaction 1 */
SELECT age FROM users WHERE id = 1;
/* will read 20 */
```
```SQL
/* Transaction 2 */
UPDATE users SET age = 21 WHERE id = 1;
/* No commit here */
```
```SQL
/* Transaction 1 */
SELECT age FROM users WHERE id = 1;
/* will read 21 */
```
```SQL
/* Transaction 2 */
ROLLBACK;
/* lock-based DIRTY READ */
```
### Non-repeatable reads
一個 transation 重複兩次查詢得到不同的結果，因為別的 transation 有修改 (UPDATE) 到這個資料
```SQL
/* Transaction 1 */
SELECT * FROM users WHERE id = 1;
```
```SQL
/* Transaction 2 */
UPDATE users SET age = 21 WHERE id = 1;
COMMIT; /* in multiversion concurrency control, or lock-based READ COMMITTED */
```
```SQL
/* Transaction 1 */
SELECT * FROM users WHERE id = 1;
COMMIT; /* lock-based REPEATABLE READ */
```
### Phantom reads
一個 transation 查詢同一個資料集拿到不同筆數，因為別的 transation 有新增、修改或刪除 (INSERT/UPDATE/DELETE) 改變了資料集
```SQL
/* Transaction 1 */
SELECT * FROM users WHERE age BETWEEN 10 AND 30;
```
```SQL
/* Transaction 2 */
INSERT INTO users VALUES ( 3, 'Bob', 27 );
COMMIT;
```
```SQL
/* Transaction 1 */
SELECT * FROM users
WHERE age BETWEEN 10 AND 30;
```
## Transaction Isolation Levels
設定用來避免或是允許 Dirty reads, Non-repeatable reads, Phantom reads
### READ UNCOMMITTED
在這個層級 DB 允許讓一個 transaction 可以讀取其他 transaction 還沒有 commit 的資料
會引發 Dirty reads, Non-repeatable reads, Phantom reads
### READ COMMITTED
在這個層級 DB 會對要改動的資料加上 WRITE_LOCK ，其他要讀取的 transation 會等到 WRITE_LOCK 解開才能動作 (Writes block Reads)
### REPEATABLE READ
在這個層級 DB 會讓要寫入/讀取的 transation lock 住這個資料如果是寫入做動和 READ COMMITTED 一樣若是讀取則會幫資料加上 READ_LOCK 其他讀取的 transation 可以一起讀取但是修改則是要等到 READ_LOCK 解開才能繼續動作 (Reads block Writes)
### SERIALIZABLE
在 REPEATABLE READ 基礎上，讀取時加上 PREDICATE_LOCK，其他 transation 只要影響到這次讀取的資料集 (INSERT/UPDATE/DELETE) 都會被鎖住直到此讀取完成，因為要計算改動是否符合讀取範圍，故很消耗 CPU，也很容易引起 deadlock，不建議使用  (Reads block Writes)
### 比較
| Isolation level | Lost updates | Dirty reads | Non-repeatable reads | Phantoms |
|-----------------|--------------|-------------|----------------------|----------|
| Read Uncommitted | don't occur | may occur | may occur | may occur |
| Read Committed | don't occur | don't occur | may occur | may occur |
| Repeatable Read | don't occur | don't occur | don't occur | may occur |
| Serializable | don't occur | don't occur | don't occur | don't occur  |



### 備註
MySQL Innodb 預設 Isolation Level 是 REPEATABLE READ
- REF:https://blog.xuite.net/vexed/tech/24805030-MySQL+%E8%88%87+Transaction+Isolation+Levels

# Durability
已經 commit 的資料不會流失（除非儲存裝置故障）
## REDO LOG
https://github.com/TritonHo/slides/blob/master/Taipei%202015-08%20course/lesson0.pdf

1. 改動資料後，發出 commit 指令。此時資料還停留在記憶體
2. 資料庫把改動過得 page 附加在 REDO log1 檔案的尾巴
3. 資料庫對作業系統發出 fsync 保證資料寫入硬碟而非檔案系統的快取中
4. 對 transation 發出 commit 成功
-- 此時資料還在 REDO log1
5. 資料庫把資料抄回它該去的地方 (DB 真正存檔的地方，IOT or heap table)
6. 確定 REDO log1 的資料都抄到正確的地方後刪除 REDO log1


# Index
http://philipzhong.blogspot.com/2011/06/how-to-improve-mysql-select-count.html
https://medium.com/d-d-mag/postgresql-%E7%95%B6%E4%B8%AD%E7%9A%84-index-e7e1e8d9340c
https://devcenter.heroku.com/articles/postgresql-indexes
https://leopard.in.ua/2015/04/13/postgresql-indexes#.W-aHnctfjmh
https://sweetness.hmmz.org/2015-05-22-block-range-brin-indexes-in-postgresql-95.html
https://medium.com/@rrfd/indexing-and-b-trees-applications-in-postgres-fdaf5cee5dba


# VS
https://yq.aliyun.com/articles/58421