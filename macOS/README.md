## Install XCode
### Open the `App Store`
### Open `Developer`
### Install `Xcode`
### Open `Xcode`
#### Click left-top corner `Xcode`
#### Select `Preferences`
#### Select `Locations`
#### Select `Xcode <version>` on the `Command Line Tools`
### Verify
```shell
xcode-select -print-path
# output
/Applications/Xcode.app/Contents/Developer
```

## Install pipx
### Install with tmp pip folder
```shell
export PYTHONUSERBASE=/tmp/pip-tmp
export PIP_CACHE_DIR=/tmp/pip-tmp/cache
pip install --disable-pip-version-check --no-warn-script-location  --no-cache-dir --user pipx
/tmp/pip-tmp/bin/pipx install --pip-args=--no-cache-dir pipx
rm -rf /tmp/pip-tmp
```
### Update .zshrc
```shell
# Pipx Config
export PIPX_HOME="$HOME/.pipx"
export PIPX_BIN_DIR="$PIPX_HOME/bin"
export PATH="$PIPX_BIN_DIR:$PATH"
```

## Elpy issue
### Error message
```text
elpy-rpc--default-error-callback: peculiar error: "exited abnormally with code 1"
```
### Fix it
#### Check virtualenv exists
```shell
virtualenv
```

#### Emacs reinstall elpy virtualenv
```text
M-x elpy-rpc-reinstall-virtualenv
```
