'''
Target:
# Clear factory can return any models clear object
Clear = ClearFactor()
clear = Clear('content')        # Use __call__ return registered object
clear('title')                  # clear function will clear whole relate field


===

Clear factory can
1. receive type return type object  (Use __call__ return registered object)
2. object can be regiser by inherit (meta class)

Every Clear object need
1. Is singleton
'''
class SingletonRegistryMetaClass(type):
    def __init__(cls, name, bases, _dict):
        super().__init__(name, bases, _dict)

        if not hasattr(cls, 'registry'):
            cls.registry = dict()
        cls.registry[cls.__name__] = cls

        original_new = cls.__new__

        def registry_new(cls, *args, **kwds):
            if cls.instance is None:
                cls.instance = dict()
            cls.instance[cls.__name__] = cls.instance.get(
                cls.__name__,
                original_new(cls, *args, **kwds)
            )
            return cls.instance[cls.__name__]

        def subclass_new(cls, *args, **kwds):
            super = cls.__bases__[0]()
            if super.instance is None:
                super.instance = dict()
            super.instance[cls.__name__] = super.instance.get(
                cls.__name__,
                object.__new__(cls, *args, **kwds)
            )
            return super.instance[cls.__name__]
        cls.instance = None

        if bases:
            cls.__new__ = staticmethod(subclass_new)
        else:
            cls.__new__ = staticmethod(registry_new)


class ClearFactor(metaclass=SingletonRegistryMetaClass):
    def __call__(cls, type):
        return cls.registry[type].__new__(cls.registry[type])

class Content(ClearFactor):
    def clear(self):
        print('I am content clear')


class Title(ClearFactor):
    def clear(self):
        print('I am title clear')


def regiser(func):
    setattr(Clear(), func.__name__, func)


# @regiser
# def title(self):
#     print('Clear title object', self.pk)


# @regiser
# def content(self):
#     print('Clear content object', self.pk)

Clear = ClearFactor()
a = Clear('Title')
b = Clear('Content')
assert a is not b
# a.title()
# a.content()
