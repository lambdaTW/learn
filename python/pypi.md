PyPI
---
# Build own pip package
## Prepare
### Register PyPI account
PyPi: https://pypi.org/account/register/
### Install required packages

- **Setuptools** for create your package
- **Wheel**,  provides a `bdist_wheel command` for **Setuptools**, let you can build `.whl` file
- **tqdm**, just a progress meter  for **Twine**
- **Twine**, let you can connect to PyPI with HTTPS

```shell
pip install --user --upgrade pip setuptools wheel
pip install --user tqdm
pip install --user --upgrade twine
```
## Create your project

### Create project and  virtual environment

```shell
mkdir pypi-test
cd pypi-test
python -m venv venv
source venv/bin/activate
git init
echo "venv" >> .gitignore
echo "build/" >> .gitignore
echo "dist/" >> .gitignore
echo "*.egg-info/" >> .gitignore
echo "__pycache__/" >> .gitignore
git commit -m "Init project" -m "Add .gitignore"
```

### Make requirement

```shell
pip install Flask
pip freeze > requirement.txt
git add .
git commit -m "Add requirement.txt"
```

### Write your project

#### Make app dir
```shell
mkdir http_ok
cd http_ok
touch __init__.py
```

#### Write the code
```python
'''/path/to/project/pypi-test/http_ok/run.py
The real http server return fake response 
'''
from flask import Flask
from werkzeug.routing import Rule
app = Flask(__name__)
app.url_map.add(Rule('/', defaults={'path': ''}, endpoint='dummy'))
app.url_map.add(Rule('/<path:path>', endpoint='dummy-path'))


@app.endpoint('dummy')
@app.endpoint('dummy-path')
def catch_all(path):
    return 'You want path: %s' % path


def main():
    global app
    app.run()


if __name__ == '__main__':
    main()
```

### Build the code as package

#### Create setup.py

```shell
cd /path/to/project/pypi-test
touch setup.py
```

#### Config setup.py

```python
from setuptools import setup


setup(
    name='http-ok',
    version='0.1',
    description='The fake HTTP server always return 200 OK',
    long_description="The fake HTTP server it always return 200 OK",
    classifiers=[
        'Programming Language :: Python :: 3.7',
    ],
    keywords='dummy fake mock http ok 200',
    url='https://gitlab.com/lambdaTW/http-ok',
    author='lambdaTW',
    author_email='lambda@lambda.tw',
    license='MIT',
    packages=['http_ok'],
    install_requires=[
        'flask',
    ],
    entry_points={
        'console_scripts': ['ok=http_ok.run:main'],
    },
    zip_safe=False
)
```

#### Build your package as wheel

```shell
# Please check your have had install wheel or you will get the command not found
cd /path/to/project/pypi-test
python setup.py bdist_wheel
```

Now there are new dir in this project root

- build: Your package will be duplicate in this dir
- dist: The wheel file will be save here
- <project_name>.egg-info: More info about this package and you can use

#### Install wheel file
```shell
# Install
pip install dist/http_ok-0.1-py3-none-any.whl
# Check package is installed
pip list
# check command
ok
# ctl+c for break
```

### Upload pip

#### Setting your .pypirc

```shell
cat << 'EOF' >> ~/.pypirc
[distutils] 
index-servers=pypi
[pypi] 
repository=https://upload.pypi.org/legacy/ 
username=<your pypi username>
EOF
```

#### Upload

```shell
cd /path/to/project/pypi-test
python -m twine upload dist/*
```

### Check package available

```shell
cd /tmp
mkdir test
cd test
python -m venv venv
source venv/bin/activate
pip install http-ok

# Test command
ok
# ctl+c for break
```


## Additional
### Add not python files
#### Add `MANIFEST.in` to your project root
MANIFEST.in
```
include README.md
recursive-include <project app> *.html
```

#### Add `include_package_data=True` in to `setup.py`
setup.py
```python
setup(
    ...,
    include_package_data=True,
)
```

# Refs
## [Django](https://docs.djangoproject.com/en/3.0/intro/reusable-apps/)
