# Flask 
## 在 `終端機` `安裝` (`pip` 是 `python` 的套件管理軟體，就是幫你下載別人寫好的軟體你就可以直接用 `import` 來使用別人的程式)
```bash
pacman -S python-pip
```

## 用 `pip` `安裝` `Flask`
```bash
sudo pip install Flask
```

## 最簡單的 hello world 使用
### 先寫一個檔 hello.py
```python
from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run()
```
### 在 `終端機` 執行它(剛剛寫的程式)
```bash
python3 hello.py
# 輸入 ctl + c ，結束程式
```

### 在瀏覽器上開啟我們寫的網頁
#### http://127.0.0.1:5000

## 從 url route (網址) 獲取參數
### 在 hello.py 上面加上一這段程式
```python
@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return 'User %s' % username

@app.route('/post/<int:post_id>')
def show_post(post_id):
    # show the post with the given id, the id is an integer
    return 'Post %d' % post_id
```
### 重啟我們的程式
```bash
# 如果你剛剛的程式還在記得關掉他喔! (ps: 輸入 ctl + c ，結束程式)
python3 hello.py
```
### 在瀏覽器上開啟我們寫的網頁
#### http://127.0.0.1:5000/user/GG
#### http://127.0.0.1:5000/user/hello
#### http://127.0.0.1:5000/user/lambda
#### http://127.0.0.1:5000/post/1
#### http://127.0.0.1:5000/post/9
#### http://127.0.0.1:5000/post/101

## 使用 `樣版` (`template`)
### 在 hello.py 上面加上一這段程式
```python
# 這行寫在最上面
from flask import render_template

@app.route('/hello/')
@app.route('/hello/<name>')
def hello_use_template(name=None):
    # 第一個參數是在 templates 資料夾下你要使用的 樣版 檔案
    # 第二個參數是你要傳給 樣版 解析代入的資料
    # 第二個參數前面的 template_name 是樣板中的變數
    # 第二個參數後面的 name 是這個 hello_use_template 函數中的變數
    return render_template('base.html', template_name=name)
```

### 在 hello.py 的目錄下加一個 templates `資料夾`
```bash
mkdir templates
```

### 在 templates 下加一個 html 檔 (本範例檔名使用 base.html)
```html
<!doctype html>
<title>Hello from Flask</title>
{% if template_name %}
  <h1>Hello {{ template_name }}!</h1>
{% else %}
  <h1>Hello, World!</h1>
{% endif %}
```
### 在瀏覽器上開啟我們寫的網頁
#### http://127.0.0.1:5000/hello/
```
因為 name 沒有東西 所以 template 會把走到 else 區塊，輸出 Hello, World!
```
```html
<!doctype html>
<title>Hello from Flask</title>

  <h1>Hello, World!</h1>
```
#### http://127.0.0.1:5000/hello/lambda
```
template 會把 template_name 帶入 name 的值，所以此時 template_name 就是 "lambda"
因為 name 有東西 所以 template 會把走到 if 區塊，輸出 Hello, lambda!
```
```html
<!doctype html>
<title>Hello from Flask</title>

  <h1>Hello lambda!</h1>
```
## 使用 `Markup` 選擇是輸出文字還是 HTML 標籤
### 在 hello.py 上面加上一這段程式
```python
@app.route('/html/br')
def br():
    return Markup('<h1>now talk about %s tag.</h1>') % '</br>'

@app.route('/html/h1')
def h1():
    return Markup.escape('<h1> h1 tag is say the content is topic title. </h1>')

@app.route('/html/none_tag')
def none_tag():
    return Markup('<p>none p tag but show html special symbol</p> &raquo; HTML').striptags()
```

### 在瀏覽器上開啟我們寫的網頁
#### http://127.0.0.1:5000/html/br
```
看原始碼你會看到
放入的 </br> 會被解析成
&lt;/br&gt;
但是 在 Markup 函式中的 <h1></h1> 不變還是長一樣，所以 h1 標籤中的內容會被瀏覽器解析成 會放大的文字 (一級標題)
```
```html
<h1>now talk about &lt;/br&gt; tag.</h1>
```
#### http://127.0.0.1:5000/html/h1
```
看原始碼你會看到
< 這個符號變成 &lt; 
> 這個符號變成 &gt;
所以 <h1></h1> 中的內容不會被瀏覽器解析成 會放大的文字 (一級標題)
```
```html
&lt;h1&gt; h1 tag is say the content is topic title&lt;/h1&gt;
```
#### http://127.0.0.1:5000/html/none_tag
```
看原始碼你會看到你在 Markup function 中的 tag 字串都被 striptags 這個函數給刪了
```
```html
none p tag but show html special symbol » HTML
```
