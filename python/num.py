from sys import argv
import operator
from itertools import permutations


MAX_STEP = 3
OPERATORS = [
    operator.add,               # A + B
    operator.sub,               # A - B
    operator.mul,               # A * B
    operator.floordiv,          # A // B
    operator.mod,               # A % B
    operator.pow,               # A ** B
]
STATIC_ARGS = list(range(-1, 11))  # -1, 0, 1, 2...10


def explain_chain(chain):
    return ', '.join([
        f'{op.__name__} {arg}'
        for op, arg in chain
    ])


def get_all_opportunity_with_arg(operator_list: list = None):
    for num_list in permutations(STATIC_ARGS, len(operator_list)):
        yield [
            (op, num)
            for op, num in zip(operator_list, num_list)
        ]


def get_opportunity_chain(num_of_func: int):
    for operator_list in permutations(OPERATORS, num_of_func):
        yield from get_all_opportunity_with_arg(operator_list)


def exec_chain(num: int, chain: list):
    for op, arg in chain:
        if op in [operator.floordiv, operator.mod] and arg == 0:
            continue
        if op in [operator.pow] and arg < 0:
            continue
        num = op(num, arg)
    return num


def valid(nums: list, chain: list) -> bool:
    for pre_num, next_num in zip(nums, nums[1:]):
        if exec_chain(pre_num, chain) != next_num:
            return False
    return True


def main(nums: list = None):
    if not nums:
        raise ValueError('Please input the list of number')

    for num_of_func in range(1, len(OPERATORS)+1):
        if num_of_func >= MAX_STEP:
            print(f'Try more than MAX_STEP ({MAX_STEP})')
            break

        for chain in get_opportunity_chain(num_of_func):
            if valid(nums, chain):
                print(f'Success get operator chain, ({explain_chain(chain)})')
                return
    print('Cannot get the operator chain')


if __name__ == '__main__':
    if argv[1:]:
        main([int(num) for num in argv[1:]])
    else:
        print('''
        Please give the serial numbers
        ex:
        python num.py 1 2 3 4 5 6
        ''')
