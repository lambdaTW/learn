# Settings DB

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },

    '<View DB>' : {
                    .......
    }
}
```

# Command Line

1. $ python manage.py inspectdb [<Table or View name>, ....] --database <View DB>

    - 反產結果直接輸出在Terminal

2. $ python manage.py inspectdb [<Table or View name>, ....] --database <View DB> > models.py

    - 反產結果直接輸出指定檔案

# Model

### This is an auto-generated Django model module.
### You'll have to do the following manually to clean this up:
###   * Rearrange models' order
###   * Make sure each model has one field with primary_key=True
###   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
###   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
### Feel free to rename the models, but don't rename db_table values or field names.

- 隨便找個欄位給他 primary_key=True，要不然 django 會噴錯
- Meta class 設 managed = False 禁用 migrate 這張 Table

# Using

```python
MdelName.objects.using('<View DB>').all()
```

