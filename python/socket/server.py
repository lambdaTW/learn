import socket, sys, time
import _thread
import subprocess
def threadWork(client):
    try:
        while True:
            msg = client.recv(1024).decode('utf-8')
            stdoutdata = subprocess.getoutput(msg)
            print("Client send: " + msg )
            client.send(("You say:"+ msg +" \r\n" + str(stdoutdata.split()) + " \r\n").encode('utf-8'))
    except BrokenPipeError:
        client.close()
    except Exception:
        import traceback
        print(traceback.format_exc())

        client.close()





try:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.timeout:
    sys.stderr.write("[ERROR] \n")
    sys.exit(1)

sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(('127.0.0.1', 54321))  # OS -> ip:port = socket
sock.listen(5)                   # split -> 5 

while True:
    (csock, adr) = sock.accept()
    print("Client Info: ", csock, adr)
    _thread.start_new_thread(threadWork, (csock, ))

sock.close()
