import socket

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    client.connect(('127.0.0.1', 54321))

    while True:
        data = input('input cmd : ')
        if data == 'exit':
            break
        client.send(data.encode('utf-8'))
        sstr = client.recv(1024)
        print(sstr.decode('utf-8'))

    client.close()
except KeyboardInterrupt:
    client.close()
except:
    import traceback
    print(traceback.format_exc())
    client.close()

client.close()
