from collections import UserList


class F(UserList):
    def __init__(self):
        self.data = [
            [1, 1],
            [1, 0]
        ]

    def matrix_self_mul(self, obj):
        return [
            [
                obj[0][0]*self[0][0]+obj[0][1]*self[1][0],
                obj[0][0]*self[0][1]+obj[0][1]*self[1][1]
            ],
            [
                obj[1][0]*self[0][0]+obj[1][1]*self[1][0],
                obj[1][0]*self[0][1]+obj[1][1]*self[1][1]
            ]
        ]

    def __mul__(self, other):
        data = self.data.copy()
        for _ in range(other):
            data = self.matrix_self_mul(data)
        return data


if __name__ == '__main__':
    # Description
    print('Show fib with matrix')
    print(
        '''
        f = F()
        print(f * 10)
        # Fib: 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89
        # [[144, 89], [89, 55]]
        '''
    )
    # Run
    f = F()
    print(f * 10)
