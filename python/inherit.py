

class Animal():
    def __init__(self, doo='do', **keyword):
        super().__init__(**keyword)
        print('I am Animal')
        self.doo = doo


class Swim():
    def __init__(self, speed=1, loop=1, **keyword):
        super().__init__(**keyword)
        print('I am Swim')
        self.speed = speed
        self.loop = loop


class Fish(Animal, Swim):
    def __init__(self):
        super().__init__(doo='doo')
        self.fish = 'ya'


f = Fish()
print(dir(f))
