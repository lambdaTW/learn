import random
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()
img_id = set()

def login():
    driver.get("https://www.shutterstock.com/base/login")
    # wait for login session
    sleep(3)
    # login
    driver.find_element_by_xpath('//*[@id="login-username"]').send_keys('yenling@avipha.com.tw')
    driver.find_element_by_xpath('//*[@id="login-password"]').send_keys('24571900ulst')
    driver.find_element_by_xpath('//*[@id="login"]').click()
    # go to history
    driver.find_element_by_xpath('/html/body/div[1]/div/article/header/div/a[2]').click()

login()

while True:
    # get img count 
    img_dir_len = len(driver.find_elements_by_xpath('//*[@id="content"]/div/ul/li/div/a'))
    # download per img in page
    try:
        for i in range(1, img_dir_len):
            xp = '//*[@id="content"]/div/ul/li[{}]/div/a'.format(str(i))
            driver.find_element_by_xpath(xp).click()
            if driver.current_url in img_id:
                driver.back()
                continue
            img_id.add(driver.current_url)
            driver.find_element_by_xpath('/html/body/div[1]/div/div/div/div[2]/form/button').click()
            # sleep like user
            sleep(random.randint(1,2))
            driver.back()
            driver.back()
    except:
        login()
        continue
    try:
        # go to next page
        driver.find_element_by_xpath('//*[@id="content"]/div/div[4]/form/button').click()
    except:
        driver.close()
