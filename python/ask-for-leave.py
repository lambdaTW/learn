# coding=utf-8
from poster.encode import multipart_encode
from poster.streaminghttp import register_openers
import getpass
import urllib2 
import urllib
import cookielib
import threading
import sys
import Queue

from HTMLParser import HTMLParser

username = raw_input("account: ")
passwd   = getpass.getpass('Password:')
target_url    = "http://ntcbadm1.ntub.edu.tw/"
target_post   = "http://ntcbadm1.ntub.edu.tw/"
absence_url   = "http://ntcbadm1.ntub.edu.tw/StdAff/STDWeb/ABS_SearchSACP.aspx"
add_pse_url   = "http://ntcbadm1.ntub.edu.tw/StdAff/STDWeb/ABS0101Add.aspx?CLIENT_ID_value=ctl00_ContentPlaceHolder1_&keepThis=true&"


class BruteParser(HTMLParser):
    
    def __init__(self):
        HTMLParser.__init__(self)
        self.tag_results = {}
        
    def handle_starttag(self, tag, attrs):
        if tag == "input":
            tag_name  = None
            tag_value = None
            for name, value in attrs:
                if name == "name":
                    tag_name = value
                if name == "value":
                    tag_value = value

            if tag_name is not None:
                self.tag_results[tag_name] = tag_value


class AbsencePaser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.record = False
        self.tem_data = []
        self.data = []

        def resets(self):
            self.record = False
            self.data.append(self.tem_data)
            self.tem_data = []

    def handle_starttag(self, tag, attrs):

        if tag == "tr":
            for name, value in attrs:
                if name == "class" and \
                   (value == "RowStyle" or value == "AlternatingRowStyle"):
                    self.record = True

    def handle_endtag(self, tag):
        if tag == "tr" and self.record:
            self.resets()
            self.record = False

    def handle_data(self, data):
        if self.record and '\n' not in data:
            self.tem_data.append(data)


def logining(opener):
    response = opener.open(target_url)
    page = response.read()
    parser = BruteParser()
    parser.feed(page)
    post_tags = parser.tag_results
    post_tags['UserID'] = username
    post_tags['PWD'] = passwd
    login_data = urllib.urlencode(post_tags)
    login_response = opener.open(target_post, login_data)
    login_result = login_response.read()

    return login_result

def logined(opener):
    if '<span id="StdNo">{}</span>'.format(username) in logining(opener):
        return opener
    else:
        return None

def absence_view(opener):
    response = opener.open(absence_url)
    page = response.read()
    parser = AbsencePaser()
    parser.feed(page)
    return opener, page

def add_pse(opener, f_date, e_date, lessons, reason_txt, reason_img):
    pass

# def main():
#     opener = register_openers()
#     opener.add_handler(urllib2.HTTPCookieProcessor(cookielib.CookieJar()))

#     logined_opener = logined(opener)
#     if logined_opener:
#         logined_opener, absence = absence_view(logined_opener)


opener = register_openers()
opener.add_handler(urllib2.HTTPCookieProcessor(cookielib.CookieJar()))

logined_opener = logined(opener)
if logined_opener:
    logined_opener, absence = absence_view(logined_opener)
