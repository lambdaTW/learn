class LazyProperty:

    def __init__(self, method):
        self.method = method
        self.method_name = method.__name__
        print('LazyProperty init')

    def __get__(self, obj, cls):
        print('LazyProperty get start')
        if not obj:
            return None
        value = self.method(obj)
        print('val {}'.format(value))
        setattr(obj, self.method_name, value)
        print('LazyProperty end')
        return value

class Test:
    def __init__(self):
        self.x = 'foo'
        self.y = 'bar'
        self._resource = None


    @LazyProperty
    def resource(self):
        print('initializing self._resource whitch is: {}'.format(self._resource))
        self._resource = tuple(range(5))
        return self._resource

t = Test()
print(t.x)
print(t.y)
getattr(t, "resource")
print(t.resource)
print(t.resource)


