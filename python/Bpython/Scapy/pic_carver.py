import re
import zlib
import cv2

from scapy.all import *

pictures_dir = "/home/justin/pic_carver/pictures"
face_dir     = "/home/justin/pic_carver/faces"
pcap_file    = "bhp.pcap"


def get_http_headers(http_payload):

    try:
        # arry.index will return index of string.
        # ex:
        # arry = ["a", "bcdasodij", "c", 1, 2, 3]
        # arry[:arry.index("a")+2]
        # out: ['a', 'bcdasodij']
        headers_raw = http_payload[:http_payload.index("\r\n\r\n")+2]

        # re.findall is regular expression search
        # dict will create dict Object ->{key:val, key:val}
        # ex:
        # str = "KEY: VAL\r\n"
        # re.findall(r"(?P<name>.*?): (?P<value>.*?)\r\n", str)
        # out:[('KEY', 'VAL')]
        # dict(re.findall(r"(?P<name>.*?): (?P<value>.*?)\r\n", x))
        # out:{'KEY': 'VAL'}
        headers = dict(re.findall(r"(?P<name>.*?): (?P<value>.*?)\r\n",
                                  headers_raw))
    except:
        return None

    # in HTTP Response headers content like
    # Content-Type: "image/gif"
    if "Content-Type" not in headers:
        return None

    return headers


def extract_image(headers, http_payload):

    image      = None
    image_type = None

    try:
        if "image" in headers['Content-Type']:
            image_type = headers['Content-Type'].split("/")[1]
            image = http_payload[http_payload.index("\r\n\r\n")+4:]

            try:
                # in HTTP Response headers content like
                # Content-Encoding: gzip

                if "Content-Encodeing" in headers.keys():
                    if headers['Content-Encodeing'] == "gzip":
                        image = zlib.decompress(image, 16+zlib.MAX_WBITS)
                    elif headers['Content-Encodeing'] == "deflate":
                        image = zlib.decompress(image)
            except:
                pass
    except:
        return None, None

    return image, image_type


def face_detect(path, file_name):
    img     = cv2.imread(path)
    cascade = cv2.CascadeClassifier("haarcascade_frontalface_alt.xml")
    rects   = cascade.detectMultiScale(img, 1.3, 4,
                                       cv2.cv.CV_HAAR_SCALE_IMAGE,
                                       (20, 20))

    if len(rects) == 0:
        return False

    rects[:, 2:] += rects[:, :2]

    for x1, y1, x2, y2 in rects:
        # select face use RGB(127, 255, 0)
        cv2.rectangle(img, (x1, y1), (x2, y2), (127, 255, 0), 2)

    cv2.imwrite("%s/%s-%s" % (face_dir, pcap_file, file_name), img)
    return True


def http_assembler(pcap_file):

    carved_img     = 0
    faces_detected = 0

    a = rdpcap(pcap_file)

    session = a.sessions()

    for session in sessions:

        http_payload = ""

        for packet in sessions[session]:
            try:
                if packet[TCP].dport == 80 or packet[TCP].sport == 80:

                    http_payload += str(packet[TCP].payload)
            except:
                pass

            headers = get_http_headers(http_payload)

            if headers is None:
                continue

            image, image_type = extract_image(headers, http_payload)

            if image is not None and image_type is not None:
                file_name = "%s-pic_carver_%d.%s" % (pcap_file, carved_img,
                                                     image_type)

                fd = open("%s%s" % (pictures_dir, file_name), "web")

                fd.write(image)
                fd.close()

                carved_img += 1

                try:
                    result = face_detect("%s%s" % (pictures_dir, file_name),
                                         file_name)

                    if result is True:
                        faces_detected += 1

                except:
                    pass
    return carved_img, faces_detected

carved_imgs, faces_detected = http_assembler(pcap_file)

print "Extracted: %d images" % carved_imgs
print "Detected: %d faces" % faces_detected
