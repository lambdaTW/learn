import urllib2 
import urllib
import cookielib
import threading
import sys
import Queue

from HTMLParser import HTMLParser

username = sys.argv[1]
passwd   = sys.argv[2]
target_url    = "http://ntcbadm1.ntub.edu.tw/"
target_post   = "http://ntcbadm1.ntub.edu.tw/"
point_url     = "http://ntcbadm1.ntub.edu.tw/STDWEB/Assessment_Main.aspx"
detail_url    = "http://ntcbadm1.ntub.edu.tw/STDWEB/Assessment_Detail.aspx"
point_val     = sys.argv[3]

class BruteParser(HTMLParser):
    
    def __init__(self):
        HTMLParser.__init__(self)
        self.tag_results = {}
        self.link = []
        self.point= {}
        
    def handle_starttag(self, tag, attrs):
        if tag == "a":
            for name, value in attrs:
                if name == "onclick" and "ShowDetail" in value:
                    self.link.append(value[12:-3].split("','"))

        if tag == "input":
            tag_name  = None
            tag_value = None
            for name, value in attrs:
                if name == "name":
                    tag_name = value
                if name == "value":
                    tag_value = value

            if tag_name is not None:
                if 'Radio' in tag_name:
                    self.tag_results[tag_name] = point_val
                else:
                    self.tag_results[tag_name] = tag_value

jar = cookielib.FileCookieJar("cookies")
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(jar))
response = opener.open(target_url)
page = response.read()
parser = BruteParser()
parser.feed(page)
post_tags = parser.tag_results
post_tags['UserID'] = username
post_tags['PWD'] = passwd
login_data = urllib.urlencode(post_tags)
login_response = opener.open(target_post, login_data)
login_result = login_response.read()

point_response = opener.open(point_url)
point_page = point_response.read()
point_paser = BruteParser()
point_paser.feed(point_page)
post_tags = point_paser.tag_results
ori_post_tags = post_tags


for course in point_paser.link:
    post_tags = ori_post_tags
    post_tags['Hide_Years'] = course[0]
    post_tags['Hide_Term'] = course[1]
    post_tags['Hide_OpClass'] = course[2]
    post_tags['Hide_Serial'] = course[3]
    post_tags['Hide_ClassShort'] = course[4]
    post_tags['Hide_TchNo'] = course[5]
    post_tags['Hide_TchName'] = course[6]
    post_tags['Hide_Cos_Name'] = course[7]
    post_tags['Hide_TermType'] = course[8]
    post_tags['Hide_SelStyle'] = course[9]

    # print("first")
    # for name, val in post_tags.iteritems():
    #     if not '__' in name:
    #         print("name",name,"value",val)

    post_data = urllib.urlencode(post_tags)
    detail_response = opener.open(detail_url, post_data)
    detail_page = detail_response.read()
    detail_paser = BruteParser()
    detail_paser.feed(detail_page)

    post_tags = detail_paser.tag_results
    post_tags['Hide_Years'] = course[0]
    post_tags['Hide_Term'] = course[1]
    post_tags['Hide_OpClass'] = course[2]
    post_tags['Hide_Serial'] = course[3]
    post_tags['Hide_ClassShort'] = course[4]
    post_tags['Hide_TchNo'] = course[5]
    post_tags['Hide_TchName'] = course[6]
    post_tags['Hide_Cos_Name'] = course[7]
    post_tags['Hide_TermType'] = course[8]
    post_tags['Hide_SelStyle'] = course[9]
    post_tags['SaveData'] = 'Y'

    # print("sec")
    for name, val in post_tags.iteritems():
        first = True
        isum = 0
        if 'Radio' in name:
            isum += 1
            if first:
                first = None
                r = str(isum) + '-' + str(point_val) + '-'
            else:
                r += ',' + str(isum) + '-' + str(point_val) + '-'

    post_tags['Hide_Str'] = r
    post_tags['Hide_Str2'] = ''

    post_data = urllib.urlencode(post_tags)
    over_response = opener.open(detail_url, post_data)
    over_page = over_response.read()


point_response = opener.open(point_url)
point_page = point_response.read()


print(point_page)
