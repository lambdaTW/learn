def my_min():
     curr = yield 0               # return 0 and wait get value for it.send() 
     val = 0
     while True:
         print('1 curr: ', curr)
         print('1 val: ', val)
         val = yield curr       # return curr value, and wait it.send() value to var val
         print('2 curr: ', curr)
         print('2 val: ', val)
         curr = min(val, curr)
         print('3 curr: ', curr)
