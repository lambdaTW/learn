* base
** array
   #+BEGIN_SRC python
     # arry.index will return index of string.
     arry = ["a", "bcdasodij", "c", 1, 2, 3]
     arry[:arry.index("a")+2]
     # out: ['a', 'bcdasodij']
   #+END_SRC
** Object
*** class                                               :metaprogramming:py3:
    #+BEGIN_SRC python
      class Meta(type):
          def __new__(mcl, name, bases, nmspc):
              print("Meta.__new__", mcl, name, bases, nmspc)
              return super(Meta, mcl).__new__(mcl, name, bases, nmspc)

          def __init__(cls, name, bases, nmspc):
              print("Meta.__init__", cls, name, bases, nmspc)
              super(Meta, cls).__init__(name, bases, nmspc)

          def __call__(cls, *args, **kw):
              print("Meta.__call__", cls, args, kw)
              return super(Meta, cls).__call__(*args, **kw)


      class SubMeta(Meta):
          def __new__(mcl, name, bases, nmspc):
              print("SubMeta.__new__", mcl, name, bases, nmspc)
              return super(SubMeta, mcl).__new__(mcl, name, bases, nmspc)

          def __init__(cls, name, bases, nmspc):
              print("SubMeta.__init__", cls, name, bases, nmspc)
              super(SubMeta, cls).__init__(name, bases, nmspc)

          def __call__(cls, *args, **kw):
              print("SubMeta.__call__", cls, args, kw)
              return super(SubMeta, cls).__call__(*args, **kw)


      class Smp(object, metaclass=SubMeta):
          def __new__(self):
              print("Smp.__new__", self)
              return super(Smp, self).__new__(self)

          def __init__(self):
              self.data = True
              print("Smp.__init__", self.data)

          def go(self):
              print("go")

      foo = Smp()


      class Single(object):
          instance = None

          def __new__(self):
              if not self.instance:
                  self.instance = super(Single, self).__new__(self)
              return self.instance

      a = Single()
      b = Single()
      assert a is b

      """
      type -> Meta -> SubMeta -> Smp
              |       |          |-> SubMeta.__new__ -> Meta.__new__ -> type.__new__ -> SubMeta.__init__ -> Meta.__init__ -> type.__init__
              |       |-> None -> just declare class SubMeta(Meta) in globals()
              |-> None -> just declare class Meta in globals()

      a = Smp()
      |-> SubMeta.__call__ -> Meta.__call__ -> (type.__call__) -> Smp.__new__ -> Smp.__init__
      """
    #+END_SRC
*** super                                                               :py2:
    #+BEGIN_SRC python
      class Mate(object):
          def __init__(self):
              print('Running Mate.__init__')
              super(Mate, self).__init__()


      class A(Mate):
          def __init__(self):
              print('Running A.__init__')
              super(A, self).__init__()


      class B(A):
          def __init__(self):
              print('Running B.__init__')
              # super(B, self).__init__()
              A.__init__(self)


      class C(A):
          def __init__(self):
              print('Running C.__init__')
              super(C, self).__init__()


      class D(B, C):
          def __init__(self):
              print('Running D.__init__')
              super(A, self).__init__()

      foo = D()
      # Running D.__init__
      # Running Mate.__init__

      """
      super(class, instance).fun -> do some function both class' parent and instance' parent haved
      """
    #+END_SRC
** switcher
*** normal
    #+BEGIN_SRC python
      def numbers_to_strings(argument):
          switcher = {
              0: "zero",
              1: "one",
              2: "two",
          }
          return switcher.get(argument, "nothing")
    #+END_SRC
*** use function
   #+BEGIN_SRC python
     def zero():
         return "zero"


     def one():
         return "one"

     switcher = {
             0: zero,
             1: one,
             2: lambda: "two",
         }

     func = switcher.get(0, lambda: "nothing")
     func()
   #+END_SRC
*** Dispatch Methods for Classes                            :metaprogramming:
    #+BEGIN_SRC python
      class Switcher(object):
          def numbers_to_methods_to_strings(self, argument):
              """Dispatch method"""
              # prefix the method_name with 'number_' because method names
              # cannot begin with an integer.
              method_name = 'number_' + str(argument)
              # Get the method from 'self'. Default to a lambda.
              method = getattr(self, method_name, lambda: "nothing")
              # Call the method as we return it
              return method()


      def number_0(self):
          return "zero"


      def number_1(self):
          return "one"


      def number_2(self):
          return "two"
    #+END_SRC
* lib
** socket
   #+BEGIN_SRC python

   #+END_SRC
** subprocess
   run shell command
   #+BEGIN_SRC python
     import subprocess

     def run_command(cmd):
         cmd = cmd.rstrip()

         try:
             output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
         except:
             output = "Failed to execute command.\r\n"

         return output
   #+END_SRC
** struct
***  Interpret strings as packed binary data
     [[https://docs.python.org/2/library/struct.html#format-characters][python-2-struct-lib]]
     #+BEGIN_SRC python
       import struct
       struct.pack("L", 0b10101000101010101000101111010101)
       # Out : '\xd5\x8b\xaa\xa8'
     #+END_SRC
** regex
   #+BEGIN_SRC python
     import re
   #+END_SRC
*** match
    #+BEGIN_SRC python
      import re
      # ()   => subpattern
      m = re.match("(http|ftp)://([^/\r\n]+)(/[^\r\n]*)?", "http://stackoverflow.com/questions/tagged/regex")
      m.group(0)                      # 'http://stackoverflow.com/questions/tagged/regex'
      m.group(1)                      # 'http'
      m.group(2)                      # 'stackoverflow.com'
      m.group(3)                      # '/questions/tagged/regex'

      # (?:) => subpattern ps:it is None capturing group
      m = re.match("(?:http|ftp)://([^/\r\n]+)(/[^\r\n]*)?", "http://stackoverflow.com/questions/tagged/regex")
      m.group(0)                      # 'http://stackoverflow.com/questions/tagged/regex'
      m.group(1)                      # 'stackoverflow.com'
      m.group(2)                      # '/questions/tagged/regex'

      f = re.match("(?:C:\\\)([^\\\]+.*\\\)(.*)?", "C:\\gg\dd\ee\ewq")
      f.group(0)                      # 'C:\\gg\\dd\\ee\\ewq'
      f.group(1)                      # 'gg\\dd\\ee\\'
      f.group(2)                      # 'ewq'
    #+END_SRC
*** findall
    #+BEGIN_SRC python
      import re
      str = "KEY: VAL\r\n"
      re.findall(r"(?P<name>.*?): (?P<value>.*?)\r\n", str)
      # out:[('KEY', 'VAL')]
      dict(re.findall(r"(?P<name>.*?): (?P<value>.*?)\r\n", x))
      # out:{'KEY': 'VAL'}
    #+END_SRC
** os
   #+BEGIN_SRC python
     import os
   #+END_SRC
*** chdir
    #+BEGIN_SRC python
      import os
      # change directory in python
      directory = "/home/justin/joomla-3.1.1"
      os.chdir(directory)
    #+END_SRC
*** walk
   #+BEGIN_SRC python
     import os
     #os.walk is walk input directory
     #ex:
     for dirPath, dirNames, fileNames in os.walk("/python/demo/"):
         print(dirPath)
         for f in fileNames:
             print(os.path.join(dirPath, f))
     #out:
     #/python/demo/walk.py
     #/python/demo/os_walk.py
     #/python/demo/root
     #/python/demo/root/1file
     #/python/demo/root/3file
     #/python/demo/root/3subDir
     #/python/demo/root/3subDir/31file
   #+END_SRC
** threading
   #+BEGIN_SRC python
     import threading
   #+END_SRC
*** Thread
    #+BEGIN_SRC python
      import threading

      threads = 10


      def fun(): pass

      for i in range(threads):
         print "Spawning thread: %d" % i
         t = threading.Thread(target=fun)
         t.start()
    #+END_SRC
** metaprogramming
   #+BEGIN_SRC python
     class RegisterLeafClasses(type):
         def __init__(cls, name, bases, nmspc):
             super(RegisterLeafClasses, cls).__init__(name, bases, nmspc)
             if not hasattr(cls, 'registry'):
                 cls.registry = set()
             cls.registry.add(cls)
             cls.registry -= set(bases) # Remove base classes
         # Metamethods, called on class objects:
         def __iter__(cls):
             return iter(cls.registry)
         def __str__(cls):
             if cls in cls.registry:
                 return cls.__name__
             return cls.__name__ + ": " + ", ".join([sc.__name__ for sc in cls])

     class Color(object, metaclass=RegisterLeafClasses):
         pass


     class Blue(Color): pass
     class Red(Color): pass
     class Green(Color): pass
     class Yellow(Color): pass
     print(Color)
     class PhthaloBlue(Blue): pass
     class CeruleanBlue(Blue): pass
     print(Color)
     for c in Color: # Iterate over subclasses
         print(c)

     class Shape(object):
         __metaclass__ = RegisterLeafClasses

     class Round(Shape): pass
     class Square(Shape): pass
     class Triangular(Shape): pass
     class Boxy(Shape): pass
     print(Shape)
     class Circle(Round): pass
     class Ellipse(Round): pass
     print(Shape)

   #+END_SRC
   #+BEGIN_SRC python
     from pprint import pprint


     class Tag1: pass


     class Tag2: pass


     class Tag3:
         def tag3_method(self): pass


     class MetaBase(type):
         def __new__(mcl, name, bases, nmspc):
             print('MetaBase.__new__\n')
             return super(MetaBase, mcl).__new__(mcl, name, bases, nmspc)

         def __init__(cls, name, bases, nmspc):
             print('MetaBase.__init__\n')
             super(MetaBase, cls).__init__(name, bases, nmspc)


     class MetaNewVSInit(MetaBase):
         def __new__(mcl, name, bases, nmspc):
             # First argument is the metaclass ``MetaNewVSInit``
             print('MetaNewVSInit.__new__')
             for x in (mcl, name, bases, nmspc):
                 pprint(x)
             print('')
             # These all work because the class hasn't been created yet:
             if 'foo' in nmspc:
                 nmspc.pop('foo')
             name += '_x'
             bases += (Tag1,)
             nmspc['baz'] = 42
             return super(MetaNewVSInit, mcl).__new__(mcl, name, bases, nmspc)

         def __init__(cls, name, bases, nmspc):
             # First argument is the class being initialized
             print('MetaNewVSInit.__init__')
             for x in (cls, name, bases, nmspc):
                 pprint(x)
             print('')
             if 'bar' in nmspc:
                 nmspc.pop('bar')    # No effect
             name += '_y'            # No effect
             bases += (Tag2,)        # No effect
             nmspc['pi'] = 3.14159   # No effect
             super(MetaNewVSInit, cls).__init__(name, bases, nmspc)
             # These do work because they operate on the class object:
             cls.__name__ += '_z'
             cls.__bases__ += (Tag3,)
             cls.e = 2.718


     class Test(object):
         __metaclass__ = MetaNewVSInit

         def __init__(self):
             print('Test.__init__')

         def foo(self): print('foo still here')

         def bar(self): print('bar still here')
   #+END_SRC
   #+BEGIN_SRC python
     class SingletonMetaClass(type):
         def __init__(cls,name,bases,dict):
             super(SingletonMetaClass,cls).__init__(name,bases,dict)
             original_new = cls.__new__
             def my_new(cls,*args,**kwds):
                 if cls.instance == None:
                     cls.instance = \
                                    original_new(cls,*args,**kwds)
                 return cls.instance
             cls.instance = None
             cls.__new__ = staticmethod(my_new)

     class bar(object, metaclass=SingletonMetaClass):
         def __init__(self,val):
             self.val = val
         def __str__(self):
             return 'self' + self.val

     x=bar('sausage')
     y=bar('eggs')
     z=bar('spam')
     print(x)
     print(y)
     print(z)
     print(x is y is z)

   #+END_SRC
** functools
*** wraps
    #+BEGIN_SRC python
      from functools import wraps
       
       
      def without_wraps(func):
          def __wrapper(*args, **kwargs):
              return func(*args, **kwargs)
          return __wrapper
       
      def with_wraps(func):
          @wraps(func)
          def __wrapper(*args, **kwargs):
              return func(*args, **kwargs)
          return __wrapper
       
       
      @without_wraps
      def my_func_a():
          """Here is my_func_a doc string text."""
          pass
       
      @with_wraps
      def my_func_b():
          """Here is my_func_b doc string text."""
          pass
       
       
      # Below are the results without using @wraps decorator
      print(my_func_a.__doc__)
      # >> None
      print(my_func_a.__name__)
      # >> __wrapper

      # Below are the results with using @wraps decorator
      print(my_func_b.__doc__)
      # >> Here is my_func_b doc string text.
      print(my_func_b.__name__)
      # >> my_func_b
    #+END_SRC
