#!/bin/bash
filename='ori'
exec < $filename

while read line
do
    echo $line # 一行一行印出內容
    svnadmin create $line # 印出 "a $line" 此行的內容, 可另外作特別處理.
    chown -R apache:apache $line/
    chcon -R -t httpd_sys_content_t $line/
    chcon -R -t httpd_sys_rw_content_t $line/
    svnadmin load $line/ < ~/dump/$line.svn
  
done
