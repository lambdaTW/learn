* use letsencrypt get new pem
certbot certonly -d l.lambda.tw

* gen clienttruststore.bks file
cd /etc/letsencrypt/archive/l.lambda.tw
keytool -importcert -trustcacerts -keystore clienttruststore.bks -storetype bks -storepass lencrypt -file cert1.pem -provider org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath /root/Downloads/bcprov-jdk15on-152.jar

* other server use pem
cert1.pem -> server.pem
privkey1.pem -> server_key.pem
