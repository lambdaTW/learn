## Create Key
```shell
# Create 256 bit (32 byte) secret key
openssl rand -out PlaintextKeyMaterial.bin 32
```

## Encrypt
```shell
openssl pkeyutl -in PlaintextKeyMaterial.bin \
                -out EncryptedKeyMaterial.bin \
                -inkey wrappingKey_<hash> \
                -keyform DER \
                -pubin \
                 -encrypt \
                -pkeyopt rsa_padding_mode:oaep \
                -pkeyopt rsa_oaep_md:sha256
```

## Upload keys
- `EncryptedKeyMaterial.bin`
- `importToken_<hash>`
