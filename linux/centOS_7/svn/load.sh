#!/bin/bash
filename='ori'
exec < $filename

while read line
do
    echo $line
    svnadmin create $line
    chown -R apache:apache $line/
    chcon -R -t httpd_sys_content_t $line/
    chcon -R -t httpd_sys_rw_content_t $line/
    svnadmin load $line/ < ~/dump/$line.svn
done
