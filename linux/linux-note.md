# just command
## get system limit
    ulimit -a

## create linux /etc/passwd 's password
    openssl passwd -l
    
## find
    find / -perm 755 -print    
# Trick
## resource lack
- disk
    1. `df -k`
    2. `du -sk *`
    3. `cd <some full path>`
    4. `loop 2. 3.`
    5. `echo "" > <fat file>`
- process
    - `top`
    - `ps -aux`
    - `kill -9 <pid>`
    - `ipcs`
    - `ipcrm id`
## linux trap
- `ls` command hook
    - create ex1.sh
        ```
        #!/bin/sh
        x=`who -m | awk '{print $6}' | tr -t "(" " "| tr -t ")" " "`
        
        y=`cat /etc/hosts | grep $x`
        
        z=`ps | grep bash | awk '{print $2}'`
        
        
        if [ "$y" == "" ]; then
            w | mail -s "Hacker comes!" admin@domain.com
            sudo /home/block.sh $x $z
            
            # echo "msg to hacker on terminal" > /dev/$z
        else
            /bin/ls.old $@
        fi
        ```
    - command: `chmod 755 ex1.sh`
    - create block.sh
        ```
        #!/bin/sh
        /sbin/iptables -A INPUT -p tcp -s $x -j DROP
        echo "ALL : "$1 >> /etc/hosts.deny
        
        for pid in `ps -ef | grep $2 | awk '{print $2}' `
        do
            kill -9 $pid
        done
        ```
    - command: `chmod 755 block.sh`
    - edit /etc/sudoers
        ```
        ALL ALL=NOPASSWD: /home/block.sh
        ```
    - commads
        - `mv /bin/ls /bin/ls.old`
        - `cp ex1.sh /bin/ls`
## xinetd as forwarding service
- file
    ```
    #/etc/xinetd.d/uuu
    service uuu
    {
    flags        = REUSE
    socket_type  = stream
    wait         = no
    user         = root
    server       = /usr/sbin/in.telnetd
    port         = 10000
    redirect     = www.google.com 80
    log_on_failure += USERID
    disable      = no
    }
    ```
- command
    ```
    systemctl restart xinetd.service
    ```
## tcp wrapper
- file
    - /etc/hosts.allow
    - /etc/hosts.deny
- example
    ```
    in.telnetd: 10.1.1.1 10.1.1.2 : allow
    in.ftpd: 10.1.1.1 10.1.1.2 : allow
    in.telnetd: ALL : DENY
    in.ftpd: ALL : DENY
    # 10.1.1 = 10.1.1.1-10.1.1.255
    in.telnetd: 10.1.1 : allow
    ALL: ALL : DENY
    # for SMTP
    in.telnetd: 10.1.1 : allow
    sendmail: ALL : allow
    ALL: ALL : DENY
    ```
## ctrl + alt + delete
### System V
    #/etc/inittab
    # disable: just comment this line 
    ca::ctrlaltdel:/sbin/shutdown -a -t3 -r now
    # -a option will check /etc/shutdown.allow
### Upstart
    #/etc/init/control-alt-delete.conf
### systemd
    # disable ctrl + alt + delete function
    systemctl mask ctrl-alt-del.target
    systemctl daemon-reload
## worms
### memory
    int main()
    {
    char *ptr;
    while(1)
    ptr = (char *)malloc(sizeof(char) * 4096);
    return 0;
    }
### cpu
    #include <stdio.h>
    #include <math.h>
    
    int main()
    {
    float x;
    int i;
    x = 0.123;
    for (;;)
        {
        x = 4*x*(1-x);
        }
    }
### process
    int main()
     {
     while(1)
     fork();
     return 0;
     }
## linux hard
### PATH
    ps 
    cp /bin/ls /usr/bin/ps
    ps
    echo $PATH
## time rotate server
### edit file
#### /root/serve
    service serve
    {
        flags          = REUSE
        socket_type    = stream
        wait           = no
        user           = root
        port           = 80
        server         = /usr/sbin/in.telnetd
        log_on_failure += USERID
    }
#### hackhttp.sh
    #!/bin/bash
    
    /sbin/service httpd stop
    cp /root/serve /etc/xinetd.d/
    /sbin/service xinetd restart
    
    sleep 300
    
    rm -f /etc/xinetd.d/serve
    /sbin/service xinetd restart
    /sbin/service httpd start
    
### command
    chmod 755 hackhttp.sh
### edit crontab file
    60 2 * * * /root/hackhttp.sh
    
# system architecture
## resource
### command
- free
    - memory & swap
- ipcs
    - shared memory
    - semaphore
    - message queues
- ipcrm
    - free or kill ipcs process
- kill
- ps
    - `ps -aux --sort +size`
- sar
    - system resource paper
- top
- vmstat
    - virtual memory statistics
### file
- /etc/sysctl.conf
- /etc/default
- /etc/network/interfaces
- /etc/security/access.conf
    - <auth>:<user>:<source>
    - in <auth> `-` is reject & `+` is accept
    - example
        ```
        -:ALL EXCEPT superman shutdown sync:LOCAL
        -:super:ALL EXCEPT LOCAL .test.com
        -:guest1 guest2 guest3:ALL
        ```
- /etc/securitylimit.conf
    - <domain> <type> <item <value>
    - in domain
        - account
        - group name, use @<group_name>
        - `*` mean default
    - in type
        - `soft` just downgrade priority
        - `hard` stop hard
    - in item
        - core : core file(KB)
        - data : data segment(KB)
        - fsize: file(KB)
        - memlock: memory(KB)
        - nofile: open number of file on same time
        - rss: daemon process(KB)
        - stack: stak(KB)
        - cpu: CPU time(MIN)
        - nproc: process number
        - as: file descriptors
        - maxlogins: login max
        - priority: excute process priority
        - locks: max file lock number
    - example
      ```
      # user in limited group
      @limited hard core 		10240
      @limited hard data 		30720
      @limited hard memlock 	30720
      @limited hard rss 		30720
      @limited hard nofile 		512
      @limited hard stack 		30720
      @limited hard cpu 		10
      @limited hard nproc 		25
      @limited hard maxlogins   5
      @limited hard as 		    51200
      
      # other user
      * hard core 		10240
      * hard memlock 	51200
      * hard rss 		51200
      * hard nofile 	1024
      * hard stack 		51200
      * hard nproc 		35
      * hard maxlogins  5
      * hard as 		102400
      ```
## grub2
- menu file /boot/grub/grub.cfg
- show hide menu use `Shift` Key
- disk (hdX,Y) mean X disk Y partition
    - X init from 0
    - Y init from 1
- when kernel upgrade just use `update-grub` command
- custom config just edit /etc/grub.d/40_custom this file will not be cover with any progam
- config file is `/etc/default/grub`
- other OS will be detect & install in grub2
- edit config file will not auto update until command `update-grub`
## log 
### log file /var/log
- shell history
    - .sh_history(ksh) 
    - .histroy(csh)
    - .bash_history(bash)
- lastlog
    - last login time and ip for each user
- wtmp
    - record login & logout info
    - some system include ftp connection info
    - command `last` can show this recod
- btmp
    - record login fail info
    - command `lastb` can show this recod
- sulog
    - record `su` command 
    - file path `/var/log/auth.log`
### log configure /etc/syslog.conf
- format
    - <info>.<level> <file>
    - info include
        - auth
        - authpriv
        - cron
        - daemon
        - kern
        - lpr
        - mail
        - mark
        - news
        - security(same as auth)
        - syslog
        - user
        - uucp
        - local0 ~ local7
    - level include
        - debug
        - info
        - notice
        - warning
        - warn(same as warning)
        - err
        - error(same as err)
        - crit
        - alert
        - emerg
        - panic(same as emerg)
        - none
    - file include
        - just file
            - /var/log/maillg
        - console
            - /dev/console
            - /dev/pts/0
- example
    ```
	authpriv.*     /var/log/secure
	mail.*         /var/log/maillog
	cron.*         /var/log/cron
	uucp,news.crit /var/log/spooler
	*.info;mail.none;authpriv.none;cron.none /var/log/messages
    ```
### logrotate
- command
    ```
    logrotate /etc/logrotate.conf
    ```
- file
    - /etc/logrotate.conf
