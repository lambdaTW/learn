# Linux kernal async methods
## poll
### Concept
```
process: Create poll read -> 
kernal: Get file description from process -> Execute hardware operation
--- BEFORE EXECUTED ---
process: Call receive data
kernal: Return ERROR
--- AFTER EXECUTED ---
process: Call receive data
kernal: Return data
```
### Pseudocode
```python
file = poll('/tmp/log', 'r')
while True:
    try:
        data = file.receive(1024)
    except:
        # It's none block
        # So you can do other things here!
        do_other_things()
    else:
        print(data)
```
## select
### Concept
```
process: Create poll read 10 file -> 
kernal: Get 10 file description from process -> Execute hardware operation
--- BEFORE ANY EXECUTED ---
process: Call receive data
kernal: Return ERROR
--- AFTER ANY EXECUTED ---
process: Call receive data
kernal: Return one file data
```
### Pseudocode
```python
# The file range
files = 10
select_files = select(files)
while True:
    try:
        file, data = select_files.receive(1024)
    except:
        # It's none block
        # So you can do other things here!
        do_other_things()
    else:
        print(data)
```

## epoll
### Concept
```
process A: Create epoll read 0-9 file, and only interesting 0-5 files -> 
kernal: Get 10 file description from process -> Execute hardware operation -> Create read/write memory mapping to hardware
process B: Interesting 0-5 files -> 
--- BEFORE ANY EXECUTED ---
process A: Call receive data
kernal: Return ERROR
process B: Call receive data
kernal: Return ERROR
--- AFTER ANY EXECUTED ---
process A: Call receive data
kernal: Return one interesting file data (Copy from memory)
process B: Call receive data
kernal: Return one interesting file data (Copy from same memory)
```
### Pseudocode
```python
all_files = [f'tmp/log{i}' for i in range(10)]
interesting_files = [f'tmp/log{i}' for i in range(5)]
epoll(all_files, interesting_files)
while True:
    try:
        file, data = select_files.receive(1024)
    except:
        # It's none block
        # So you can do other things here!
        do_other_things()
    else:
        print(data)
```

## linux-aio
### Concept
```
Linux-aio only works for O_DIRECT files, meaning not support memory mapping.
Process: Subscribe read file
Kernal: Receive informartion and execute hardware
Process: Do otherthings
--- AFTER PROCESS ---
Kernal: Send notification to Process
```
### Pseudocode
```python
file = aio.read('/tmp/log', 1024, )
# It's none block
# So you can do other things here!
while True:
    signal = get_signal_from_kernal()
    if signal == 1:
        print(file.data)
```
# Another async methods
## io_uring
https://www.scylladb.com/2020/05/05/how-io_uring-and-ebpf-will-revolutionize-programming-in-linux/
