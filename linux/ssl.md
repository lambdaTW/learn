## Show pem expird date
```shell
openssl x509 -enddate -noout -in <pem file>
```

## Check the pairing
```
openssl x509 -noout -modulus -in ~/ssl/<pem file> | openssl md5
# (stdin)= 0fc19bca2d2a6e20d1bafc871d6ad962
openssl rsa -noout -modulus -in ~/ssl/<key file> | openssl md5
# (stdin)= 0fc19bca2d2a6e20d1bafc871d6ad962

# In above case the checksum are same, so the key and pem are paired
```
