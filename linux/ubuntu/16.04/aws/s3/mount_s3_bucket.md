Mount S3 bucket on Ubuntu 16.04
===
## Install s3fs
```shell-script
sudo apt-get install automake autotools-dev \
fuse g++ git libcurl4-gnutls-dev libfuse-dev \
libssl-dev libxml2-dev make pkg-config

git clone https://github.com/s3fs-fuse/s3fs-fuse.git
cd s3fs-fuse
./autogen.sh
./configure
make
sudo make install
```
## Create Password File
### System Level
```shell-script
sudo vi /etc/passwd-s3fs
# echo "<ACCESS KEY ID>:<Secret Access KEY ID>" > /etc/passwd-s3fs
sudo chmod 640 /etc/passwd-s3fs
```

### User Level
```shell-script
vi ${HOME}/.passwd-s3fs
# echo ACCESS_KEY_ID:SECRET_ACCESS_KEY > ${HOME}/.passwd-s3fs
chmod 600 ${HOME}/.passwd-s3fs
```

## Mount
```shell-script
export S3_DIR=<dir_path>
export S3_BUCKET=<bucket name>
sudo mkdir -p $S3_DIR && sudo s3fs -o $S3_DIR $S3_BUCKET

# Check
df -h
```

## Umount
```shell-script
sudo umount $S3_DIR
```

## Setting `fstab`
```shell-script
sudo echo "s3fs#S3_BUCKET S3_DIR fuse _netdev,allow_other 0 0" >> /etc/fstab
```
