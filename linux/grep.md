# find string in whole path of file
```
grep -rnw '/path/to/somewhere/' -e "pattern"
```

# Along with these, --exclude, --include, --exclude-dir or --include-dir parameters could be used for efficient searching
## This will only search through the files which have .c or .h extensions
```
grep --include=\*.{c,h} -rnw '/path/to/somewhere/' -e "pattern"
```
## This will exclude searching all the files ending with .o extension
```
grep --exclude=*.o -rnw '/path/to/somewhere/' -e "pattern"
```

## --exclude-dir and --include-dir
```
grep --exclude-dir={dir1,dir2,*.dst} -rnw '/path/to/somewhere/' -e "pattern"
```
