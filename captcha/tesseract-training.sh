# just set the fontyp to your new font name (use replace)
cp ../t/t.tif langyp.fontyp.exp0.tif
tesseract langyp.fontyp.exp0.tif langyp.fontyp.exp0 -l eng -psm 7 batch.nochop makebox
cp ../t/t.box langyp.fontyp.exp0.box 
echo fontyp 0 0 0 0 0 >font_properties
tesseract langyp.fontyp.exp0.tif langyp.fontyp.exp0 -l eng -psm 7 nobatch box.train
unicharset_extractor langyp.fontyp.exp0.box 
shapeclustering -F font_properties -U unicharset -O langyp.unicharset langyp.fontyp.exp0.tr
mftraining -F font_properties -U unicharset -O langyp.unicharset langyp.fontyp.exp0.tr
cntraining langyp.fontyp.exp0.tr
mv normproto fontyp.normproto
mv inttemp fontyp.inttemp
mv pffmtable fontyp.pffmtable 
mv unicharset fontyp.unicharset
mv shapetable fontyp.shapetable
combine_tessdata fontyp.
sudo cp fontyp.traineddata /usr/share/tessdata/
