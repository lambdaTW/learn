/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.math.BigInteger;
import java.util.Random;

/**
 *
 * @author user1
 */
public class RSA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // big prime from random set p q
        BigInteger p = BigInteger.probablePrime(2048, new Random(1));
        BigInteger q = BigInteger.probablePrime(2048, new Random(2));
        BigInteger N = p.multiply(q);
        //let r = (p-1)*(q-1)
        BigInteger r = p.subtract(new BigInteger("1")).multiply(q.subtract(new BigInteger("1")));
        
        //set e , let gcd(e,r)=1
        BigInteger e=new BigInteger("30780993817193880963403466329002541102223785636595735505305793013088158003445362478837477359218246093218979047761453423434215167557851507617981435313591909729274563817325915586823459210875686996758933889657354619442277123774886416524009795315919853094349927522179025460916106636178877221352390189571172882105721296111126135594360824208544831104821968522398550247728494848325595830919691689503103318021037496912623954928763297545812540417985392550915266325706877204555648896519969640315997954692591321899758118321241771930660211085016149098838579509355963675779760846316406056441186189859323957286467825635050311538279");
        
        //set d =Modular_inverse of (e,r) , let e*d= 1 (mod r)
        BigInteger d=e.modInverse(r);
        System.out.println("p="+p);
        System.out.println("q="+q);
        System.out.println("N="+N);
        System.out.println("r="+r);
        //System.out.println(BigInteger.valueOf(42).modInverse(BigInteger.valueOf(2017)));
        System.out.println("d="+d);
        
        BigInteger m = BigInteger.valueOf(123);
        System.out.println("m="+m);
        
        BigInteger c = m.modPow(e, N);
        
        System.out.println("c="+c);
        
        BigInteger m2 = c.modPow(d, N);
        System.out.println("m2="+m2);
    }
    
    
}
