## Debug container
### Show create every stage info
```
kubectl get deployment <deployment name> -o yaml
```

### Find the error event
```shell
kubectl get events --all-namespaces
```

### Show single pod event
```shell
kubectl describe pod <not READY pod name>
```

### Show pod log
```shell
kubectl logs <pod name>
```

### Access the pod
```shell
kubectl exec -it <pod name> -- /bin/bash
```

## Debug network
### Check DNS
```
kubectl get pods
kubectl exec -ti <pod name> -- nslookup kubernetes.default
kubectl exec <your-pod-name> cat /etc/resolv.conf
```

## Operator
### Force rebuild deployment's pod
```
kubectl rollout restart deployment <deployment_name>
```
