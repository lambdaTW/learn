## Install
```shell
# Apply online configuration
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml
# Apply local configuration (version: 2019/09/18)
kubectl apply -f mandatory.yaml
```

### Minikube
```shell
minikube addons enable ingress
```
