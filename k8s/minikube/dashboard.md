## Get dashboard URL for local connection
```shell
minikube dashboard --url &

# Output
# http://127.0.0.1:<port>/api/v1/namespaces/kube-system/services/http:kubernetes-dashboard:/proxy/
```

## Proxy the dashboard for external use
```shell
kubectl proxy --address='0.0.0.0' --port=8002 --accept-hosts='^*$'

# For external machine use
http://<kube admin ip>:8002/api/v1/namespaces/kube-system/services/http:kubernetes-dashboard:/proxy/
```
