FOR /F "tokens=1-4 delims=:." %%a IN ("%time%") DO (
	SET _MyHour=%%a
)


FOR /F "tokens=1-4 delims=/ " %%a IN ("%date%") DO (
	SET _MyDate=%%a%%b%%c
)

if exist J:\%_MyDate%-db-backup-2.7z (
	exit
) else (
	if exist J:\%_MyDate%-db-backup-1.7z (
		SET _Count=2
		echo %_Count%
	) else (
		SET _Count=1
		echo %_Count%
	)
goto backup
)

:backup
"C:\Program Files (x86)\winSCP\winscp.exe" /console /script="C:\backup\sync.txt"

7za a -t7z J:\%_MyDate%-db-backup-%_Count%.7z D:\db_backup\*
	
del D:\db_backup\* /Q
FOR /D %p IN ("D:\db_backup\*.*") DO rmdir "%p" /s /q
