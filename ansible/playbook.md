## Examples
### Multi group
playbook.yml
```yaml
---
- name: Install applications
  hosts: webserver
  become: true
  tasks:
    - name: Install git
      apt: name=git state=present

    - name: Install nginx
      apt: name=nginx state=present

- name: Install applications
  hosts: dbserver
  become: true
  tasks:
    - name: Install mysql
      apt: name=mysql state=present
```
### Install packages
playbook.yml
```yaml
- name: Install applications
  hosts: all
  become: true
  tasks:
    - name: Install vim
      apt: name=vim state=present
      tags:
        - vim
    - name: Install screen
      apt: name=screen state=present
      tags:
        - screen
```
Run with tag filter
```shell-script
ansible-playbook my_playbook.yml --tags "vim,screen"
ansible-playbook my_playbook.yml --tags "screen"
ansible-playbook my_playbook.yml --skip-tags "vim"
```

### Install git
playbook.yml
```yaml
- name: Configure webserver with git
  hosts: webserver
  become: true
  vars:
    package: git
  tasks:
    - name: install git
      apt: name={{ package }} state=present
```
## Task
### become
Change user on the task, default is become the root
#### Only in a task
playbook.yml
```yaml
- name: Run script as foo user
  command: bash.sh
  become: true
  become_user: foo
```
#### Run a role as root
playbook.yml
```yaml
- hosts: all
  roles:
    - { role: myrole, become: yes }
    - myrole2
```
#### Run all role tasks as root
playbook.yml
```yaml
- hosts: all
  become: true

- name: Start apache
  service: apache2
  state: started
```

### when
example.yml
```yaml
when:
  ansible_distribution in ['RedHat', 'CentOS', 'ScientificLinux'] and
  (ansible_distribution_version|version_compare('7', '<') or
  ansible_distribution_version|version_compare('8', '>='))
  or
  ansible_distribution == 'Fedora'
  or
  ansible_distribution == 'Ubuntu' and
  ansible_distribution_version|version_compare('15.04', '>=')
```

### with_items
#### predefined list
group_vars/develop.yml
```yaml
favorite_snacks:
  - hotdog
  - ice cream
  - chips
```
playbook.yml
```yaml
- name: create directories for storing my snacks
  file: path=/etc/snacks/{{ item }} state=directory
  with_items: '{{ favorite_snacks }}'
```
#### predefined dictionary
group_vars/develop.yml
```yaml
packages:
  - name: tree
    state: present
  - name: nmap
    state: present
  - name: apache2
    state: absent
```

playbook.yml
```yaml
- name: manage packages
  package: name={{ item.name }} state={{ item.state }}
  with_items: '{{ packages }}'
```
