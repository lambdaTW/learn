## Role structure
```
apache/
├── defaults
│   └── main.yml
├── vars
│   ├── Debian.yml
│   └── RedHat.yml
├── tasks
│   ├── setup-RedHat.yml
│   ├── setup-Debian.yml
│   ├── main.yml
│   ├── configure-RedHat.yml
│   └── configure-Debian.yml
├── files
│   ├── mod-pagespeed-stable_current_i386.deb
│   ├── mod-pagespeed-stable_current_i386.rpm
│   ├── mod-pagespeed-stable_current_amd64.deb
│   └── mod-pagespeed-stable_current_x86_64.rpm
├── templates
│   ├── httpd.conf.j2
│   └── sites-available
│       └── virthualhost.conf.j2
├── meta
│   └── main.yml
└── handlers
    └── main.yml
```
1. `defaults`: (variable) contains default variables for the role. Variables in default have the lowest priority so they are easy to override.
2. `vars`: (variable) contains variables for the role. Variables in vars have higher priority than variables in defaults directory.
3. `tasks`: (task) contains the main list of steps to be executed by the role.
4. `files`: (file) contains files which we want to be copied to the remote host. We don’t need to specify a path of resources stored in this directory.
5. `templates`: (template) contains file template which supports modifications from the role. We use the Jinja2 templating language for creating templates.
6. `meta`: (meta) contains metadata of role like an author, support platforms, dependencies.
7. `handlers`: (task) contains handlers which can be invoked by “notify” directives and are associated with service.

### Fast create role structure
```shell-script
cd my_ansible_project/roles && ansible-galaxy init <role name>
```

### Defaults
main.yml
```yaml
---
apache_enablerepo: ""

apache_listen_ip: "*"
apache_listen_port: 80
apache_listen_port_ssl: 443

apache_vhosts_template: "vhosts.conf.j2"
apache_vhosts_filename: "vhosts.conf"
apache_packages_state: present
```

### Vars
Debian.yml
```yaml
---
__apache_packages:
  - apache2
  - apache2-utils

apache_conf_path: /etc/apache2
```

### tasks
main.yml
```yaml
---
# Include variables and define needed variables.
- name: Include OS-specific variables.
  include_vars: "{{ ansible_os_family }}.yml"

# if ansible_os_family is `Debian`
# Run setup-Debian.yml in tasks folder
- include_tasks: "setup-{{ ansible_os_family }}.yml"
```
setup-Debian.yml
```yaml
---
- name: Update apt cache.
  apt: update_cache=yes cache_valid_time=3600

- name: Ensure Apache is installed on Debian.
  apt: "name={{ apache_packages }} state={{ apache_packages_state }}"
```

### files
mod-pagespeed-stable_current_i386.deb

#### Copy file to remote server
```yaml
- name: Copy nginx files
  copy:
    src: "{{ role_path }}/files/mod-pagespeed-stable_current_i386.deb"
    dest: /tmp/mod-pagespeed-stable_current_i386.deb
    mode: 0644
```

### templates
templates/httpd.conf.j2
```j2
{{ apache_global_vhost_settings }}

{# Set up VirtualHosts #}
{% for vhost in apache_vhosts %}
<VirtualHost {{ apache_listen_ip }}:{{ apache_listen_port }}>
  ServerName {{ vhost.servername }}
{% if vhost.serveralias is defined %}
  ServerAlias {{ vhost.serveralias }}
{% endif %}
{% if vhost.documentroot is defined %}
  DocumentRoot "{{ vhost.documentroot }}"
{% endif %}
```
#### Use template in `task`
tasks/configure-Debian.yml
```yaml
- name: Add apache vhosts configuration.
  template:
    src: "{{ apache_vhosts_template }}"
    dest: "{{ apache_conf_path }}/sites-available/{{ apache_vhosts_filename }}"
    owner: root
    group: root
    mode: 0644
  notify: restart apache
  when: apache_create_vhosts | bool
```

### meta
meta/main.yml
```yaml
---
dependencies: []

galaxy_info:
  author: geerlingguy
  description: Apache 2.x for Linux.
  company: "Midwestern Mac, LLC"
  license: "license (BSD, MIT)"
```
#### Role dependencies
meta/main.yml
```yaml
dependencies:
  - { role: common, some_parameter: 3 }
  - { role: sshd, enable_sshd: false,
                         when: environment == 'production' }
```
### handlers
handlers/main.yml
```
---
- name: restart apache
  service:
    name: "{{ apache_service }}"
    state: "{{ apache_restart_state }}"
```

#### Use `notify` to run `handler` from `task`
task/configure-Debian.yml
```yaml
- name: Add apache vhosts configuration.
  template:
    src: "{{ apache_vhosts_template }}"
    dest: "{{ apache_conf_path }}/sites-available/{{ apache_vhosts_filename }}"
    owner: root
    group: root
    mode: 0644
  notify: restart apache
  when: apache_create_vhosts | bool
```
