## Directory Layout
[Reference](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html)
```
inventories/
   production/
      hosts               # inventory file for production servers
      group_vars/
         group1.yml       # here we assign variables to particular groups
         group2.yml
      host_vars/
         hostname1.yml    # here we assign variables to particular systems
         hostname2.yml

   staging/
      hosts               # inventory file for staging environment
      group_vars/
         group1.yml       # here we assign variables to particular groups
         group2.yml
      host_vars/
         stagehost1.yml   # here we assign variables to particular systems
         stagehost2.yml

library/                  # if any custom modules, put them here (optional)
module_utils/             # if any custom module_utils to support modules, put them here (optional)
filter_plugins/           # if any custom filter plugins, put them here (optional)

site.yml                  # master playbook
webservers.yml            # playbook for webserver tier
dbservers.yml             # playbook for dbserver tier

roles/                    # you can reference the role.md for sub directory layout
    common/
    webtier/
    monitoring/
    fooapp/
```
## Ansible Config
### Ansible Configuration Settings
#### The configuration file
Changes can be made and used in a configuration file which will be searched for in the following order:
- ANSIBLE_CONFIG (environment variable if set)
- ansible.cfg (in the current directory)
- ~/.ansible.cfg (in the home directory)
- /etc/ansible/ansible.cfg

#### Defualt `ansible.cfg`
[Reference](https://github.com/ansible/ansible/blob/devel/examples/ansible.cfg)
#### Setting Inventory
```
# ansible.cfg
[default]
inventory      = ./hosts    # It can use relative path
inventory      = /tmp/hosts # It can use full path
```

### Inventory
#### Example
```
# Consolidation of all groups
[hosts:children]
web-servers
offsite
onsite
backup-servers

[web-servers]
server1 ansible_host=192.168.0.1 ansible_port=1600
server2 ansible_host=192.168.0.2 ansible_port=1800

[offsite]
server3 ansible_host=10.160.40.1 ansible_port=22 ansible_user=root
server4 ansible_host=10.160.40.2 ansible_port=4300 ansible_user=root

# You can make groups of groups
[offsite:children]
backup-servers

[onsite]
server5 ansible_host=10.150.70.1 ansible_ssh_pass=password

[backup-servers]
server6 ansible_host=10.160.40.3 ansible_port=77
```

#### Use Group Vars
##### Path
```shell-script
tree
: '
project/
  group_vars/
     development
  inventory.development
  playbook.yml
'
```

##### inventory.development
```
[development]
dev.fakename.io

[development:vars]
ansible_host: 192.168.0.1
ansible_user: dev
ansible_pass: pass
ansible_port: 2232

[api:children]
development
```
##### development
```
---
app_name: NewApp_Dev
app_url: https://dev.fakename.io
app_key: f2390f23f01233f23f
```
##### playbook.yml
```yaml
---
- name: Install api.
  hosts: api
  gather_facts: true
  sudo: true
  tags:
    - api
  roles:
    - { role: api,         tags: ["api"]         }
```

### Playbook
Simple playbook
```yaml
---
- hosts: MACHINE_NAME
  tasks:
    - name: Say hello
      debug:
        msg: 'Hello, World'
```

### Security (`Vault`)
#### Create the encrypted file
```yaml
ansible-vault edit example.yml
```

#### Create password
```shell-script
pwgen 256 1 > vault_pass_file
```

#### Encrypt the plain file
```shell-script
export ANSIBLE_VAULT_PASSWORD_FILE=vault_pass_file
ansible-vault encrypt group_vars/group.yml
```

#### Decrypt
```shell-script
export ANSIBLE_VAULT_PASSWORD_FILE=vault_pass_file
ansible-vault decrypt group_vars/group.yml
```

#### Edit the encrypted file
```shell-script
export ANSIBLE_VAULT_PASSWORD_FILE=vault_pass_file
ansible-vault edit group_vars/group.yml
```

#### Run playbook with encrypted file
```shell-script
export ANSIBLE_VAULT_PASSWORD_FILE=vault_pass_file
ansible-playbook -i inventories/nodes my-playbook.yml
```

#### Decrypt the template when runing task
group_vars/all.yml
```yaml
---

view_encrypted_file_cmd: "ansible-vault --vault-password-file {{ lookup('env', 'ANSIBLE_VAULT_PASSWORD_FILE') }} view"
```

playbook.yml
```yaml
---

- name: Decrypt template
  local_action: "shell {{ view_encrypted_file_cmd }} {{ role_path }}/templates/template.enc > {{ role_path }}/templates/template"
  changed_when: False

- name: Deploy template
  template:
    src=templates/template
    dest=/home/user/file

- name: Remove decrypted template
  local_action: "file path={{ role_path }}/templates/template state=absent"
  changed_when: False
```


#### Decrypt the non-structured vault-encrypted data
playbook.yml
```yaml
---

- name: Copy private key to destination
  copy:
    dest=/home/user/.ssh/id_rsa
    mode=0600
    content=lookup('pipe', 'ANSIBLE_VAULT_PASSWORD_FILE=vault_pass_file ansible-vault view keys/private_key.enc')
```
