## Check inventory file
```shell-script
ansible-inventory -i <inventory file> --list
```

## Ping
```shell-script
ansible <group name> -m ping
```
### All
```shell-script
ansible all -m ping
```

## Show machine info
```shell-script
ansible -m setup <group name>
```
### Filter special info
```shell-script
# Get OS info
ansible all -m setup -a 'filter=ansible_os_family'
```

## Run playbook
### Static inventory
```shell-script
ansible-playbook -i path/to/static-inventory-file -l myhost myplaybook.yml
```

### Dynamic inventory
```shell-script
ansible-playbook -i path/to/dynamic-inventory-script.py -l myhost myplaybook.yml
```

### Create role
```shell-script
ansible-galaxy init <role name>
```
