#!/usr/bin/ruby
# -*- coding: utf-8 -*-
# if nil
#   p("true")
# else
#   p("nil")
# end

# p("nil") if 1 == 1

# def hello(v)
#   p(v)
# end

# p (1..100).to_a.sample(5)

# a = ["a", "b", "c"]
# p a[1..2]

# p a.first
# p a.last

# a.inject(:+)

# a.inject do |sum, n|
#   sum + n
# end

# a.select do |x|
#   x % 2 != 0
# end

# a.to_a.each do |c|
#   p c
# end

# hash = {name:"kk", age:18}
# hash.keys
# hash.values

# p hash[:name]

# hash["name"] = "string"
# p hash

# hash.each do |k, v|
# p "key = #{k}, value = #{v}"
# end
# list = [1, 2, 1, 2, 2, 2, 3, 1, 3, 4, 5, 9]
# p list.count(2)

# # set hash init with 0
# m = Hash.new(0)
# list.each do |v|
#   m[v] = m[v] + 1;
# end

# p m
# a  = ARGV[0].to_i
# op = ARGV[1]
# b  = ARGV[2].to_i

# case op
# when "+"
#   p a + b
# when "-"
#   p a - b
# else
#   p "unknow"
# end
# line_long = File.readlines("homework-03-31-content.txt").count.to_s.length
# out=""
# sum = 0
# File.readlines("homework-03-31-content.txt").each do |line|
#   out += "#{sum += 1}#{" " * (line_long - sum.to_s.length + 1)}#{line}"
# end

# puts out
# f = File.open("content.txt", "r")
# sum = 0
# f.each_line do |line|
#   sum += 1
#   p "#{sum}#{" " * (line_long - sum.to_s.length + 1)}#{line}"
# end

# f.close
# "hello, world. this is a test# comment".gsub(/[,\.#]/,"")

# def hello(name)
#   puts "hi, #{name}"
# end

# hello("kk")

# def say_hello_to(name, *options)
#   p "hi, #{name} #{options}"
# end

# say_hello_to "man", 2, 3, 45
# X=9
# def calc(x:0, y:0, z:0, r:0, g:1)
#   p [X, y, z, r, g]
# end

# calc(x:1,g:90)

# def say_hello_to a, b, c: "c", d: :d
#   p a
#   p b
#   p c
#   p d
# end

# say_hello_to("kk", c:18, b:"b",name: "name", key: "key")

# def age()
#   if 1 == 1 then
#     20
#   end
# end

# p age()def age()
#   if 1 == 1 then
#     20
#   end
# end

# p age()
# def dooo()
#   p "1"
#   yield
#   p "0"
# end

# dooo do
#   p "dooo"
#   dooo do
#     p "doooooo"
#   end
# end

# def dooo(list)
#   p "1"
#   yield list
#   p "0"
# end

# dooo([1,2,3]) do |x|
#   p x
# end
# def count_to(n)
#   (1..n).to_a.each do |k|
#     yield k
#   end
# end

# count_to(5) do |i|
#   puts "hello #{i}"
# end

# def count_to(n)
#   if block_given?
#     yield n
#   else
#     nil
#   end
# end

# count_to 5 do |k|
#   p k
# end
# def is_adult?(age)
#   age >= 18
# end

# is_adult?(2)
# is_adult?(20)

# require "./lib.rb"
# hello_lib

# def link_to act, path, arg
#   p(act)
#   p(path)
#   p(arg)
# end

# def link_to act, path, class: nil, method: nil, confirm: nil
#   p(act)
#   p(path)
#   p class:
#   p(confirm)
#   p(method)
# end

# def link_to act, path, method: nil, confirm: nil
#   p(act)
#   p(path)
#   p(confirm)
#   p(method)
# end

# def say_hello_to a, b, c: "c", d: :d
#   p a
#   p b
#   p c
#   p d
# end

# say_hello_to("kk", c:18, b:"b",name: "name", key: "key")
# class Animal
#   def sleep
#     p "zzzz"
#   end
# end

# class Dog < Animal
# end

# class Cat < Animal
#   # attr_reader :name
#   # attr_writer :name
#   attr_accessor :name
#   def initialize name, age
#     @name = name
#     @age = age
#   end

#   def greeting
#     p "hi, I'm #{@name}"
#   end

#   # def name
#   #   p @name
#   # end

#   # def name=(new_name)
#   #   @name = new_name
#   # end
# end

# kitty = Cat.new("yee", 19)
# kitty.greeting
# p kitty.name
# kitty.name= "woo"
# p kitty.name
# module Eatable
#   def eat something
#     p "eat #{something}"
#   end
# end

# class Girl
#   include Eatable
#   def initialize age
#     @age = age
#   end

#   def age
#     if @age < 18
#       @age
#     else
#       18
#     end
#   end

#   def real_age
#     @age
#   end
# end

# gg = Girl.new(10)
# puts gg.age
# ol = Girl.new(30)
# puts ol.age
# puts ol.real_age
# puts ol.eat "food"

# p String.instance_methods
# p String.instance_variables

# class Fixnum
#   alias :old_p :+
#     def + a
#         puts "hey"
#         return self.old_p a
#     end
# end
# p 1 + 12


# class Integer
#   def hex
#     p "#{self.to_s(16)}"
#   end
# end

# 134.hex

# class Cat
#   def hello
#     p "hello"
#     gossip
#   end

#   private
#   def gossip
#     p "secret!"
#   end
# end

# kitty = Cat.new
# kitty.hello

# class Cat
#   private
#   def gossip
#     p "secret!"
#   end
# end

# k = Cat.new
# k.send(:gossip)


# 5.times do
#   p "ruby"
# end

# 5.times do |x|
#   p "#{x+1}-ruby"
# end

# 1.upto(5) do |x|
#   p "#{x}-ruby"
# end

# name = "aa"

# 1.times do |x|
#   name = "KK"
#   puts "hi #{name}"
# end

# puts name

# name = "aa"
# tmp = 1000
# 1.times do |x; tmp|
#   name = "KK"
#   tmp = "GG"
#   puts "hi #{name} you #{tmp}"
# end

# puts name
# puts tmp

# Tax = {tw: 0.05, jp: 0.08, us: 0.12}

# class Product
#   attr_reader :title, :price, :country, :is_online
#   def initialize title, price, country, is_online = false
#     @title = title
#     @price = price
#     @country = country
#     @is_online = is_online
#   end

#   def tax
#     Tax[country]
#   end

#   def total
#     price * (1 + tax)
#   end
  
#   def to_s
#     "title:#{title}"
#   end
# end

# p1 = Product.new("ruby", 100, :jp, true)
# p2 = Product.new("php", 200, :tw, true)
# p3 = Product.new("asp", 300, :tw)
# p4 = Product.new("java", 500, :us)
# p5 = Product.new("python", 800, :us, true)
# products = [p1, p2, p3, p4, p5]

# products.each do |product|
#   p product.total
# end

# puts products
# puts p1.total

# products.inject(0) do |sum, p|
#   p.total
# end

# products.select do |p|
#   p.country == :tw
# end.inject(0) do |sum, p|
#     sum + p.total
# end

# expensive_p=
#   products.select do |p|
#   p.total > 500
# end

# cheap_p = products - expensive_p

# high_p, low_p = products.partition {|p| p.total>500}




# class Cart
#   include Enumerable

#   def initialize
#     @items = []
#   end
  
#   def add_time product
#     @items << product
#   end

#   def each
#     @items.each do |t| yield t end
#   end
# end

# cart = Cart.new
# cart.add_time(p1)
# cart.add_time(p2)
# cart.add_time(p3)
# cart.add_time(p4)
# cart.add_time(p5)

# cart.each do |n|
#   p n.price
# end

# high, low = cart.partition do |n|
#   n.total > 300
# end

# p "HIGHT:#{high}"
# p "LOW  :#{low}"



# class Fixnum
#   def my_times
#     i = 0
#     while i < self
#       yield i
#       i += 1
#     end
#   end
# end


# class Cat
#   def Cat.fly # alias self.fly
#     puts "I can fly"
#   end
# end

# Cat.fly

# class Cat
#   class << self
#     def fly
#       puts "I can fly"
#     end
#   end
# end

# Cat.fly
# class Animal
#   def fly
#     puts "father"
#   end
# end

# module Fly
#   def fly
#     puts "include"
#   end
# end

# module Flyable
#   def fly
#     puts "extend"
#   end
# end

# class Cat < Animal
#   include Fly
#   extend Flyable

#   def fly
#     puts "my"
#   end
    
# end

# k = Cat.new
# k.fly


# def is_a_good_nubmer? n
#   result = yield n
#   if result
#     puts "is a good nubmer"
#   else
#     puts "not"
#   end
# end


# is_a_good_nubmer?(88) do |x|
#   x == 88
# end
# class Array
#   def my_select
#     select = []
#     self.each do |n|
#       result = yield n
#       if result
#         select << n
#       end
#     end
#     select
#   end
# end

# [*1..100].my_select do |x|
#   x.odd?
# end


# require "amd"

# class Cat
#   include Amd
# end

# kitty = Cat.new
# kitty.fly
# def bmi_calculator(height, weight)
#   re = weight / ((height / 100.0) ** 2)
#   return format("%.2f",re).to_f
# end

# puts bmi_calculator(178, 70)

# class Movie
#   attr_accessor :name, :price
#   def initialize name, price
#     @name = name
#     @price = price
#   end
# end

# class Rental
#   include Enumerable
  
#   def initialize 
#     @items = []
#   end

#   def add_movie movie
#     @items << movie
#   end

#   def summary
#     summary = 0
#     @items.each do |item|
#       summary += item.price
#     end
#     "你總共租了 #{@items.length} 部電影，消費金額為 #{summary} 元"
#   end
# end

# dragon_ball = Movie.new("七龍珠", 100)  # 租金 = 100 塊
# naruto = Movie.new("火影忍者", 80)      # 租金 = 80 塊

# rental = Rental.new
# rental.add_movie(dragon_ball)
# rental.add_movie(naruto)
# puts rental.summary

# class Animal
#   def sleep
#     puts "zzzzzZZZ"
#   end
# end

# class Cat < Animal
# end

# class Dog < Animal
# end

# lucky = Dog.new
# kitty = Cat.new

# lucky.sleep
# kitty.sleep
