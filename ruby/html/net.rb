# -*- coding: utf-8 -*-
require 'net/http'
require 'open-uri'
require 'json'

class Web
  def header
    {'Content-Type' => 'text/html; charset=utf-8'}
  end

  private
  def get_data
    url = URI("http://ins-info.ib.gov.tw/opendata/json-07010801.aspx")
    raw_data = Net::HTTP.get(url)

    json_data = JSON.parse(raw_data).map do |raw|
      %Q'{"name":#{raw["INSURER_Name"]},"size":#{raw["Income"].to_f}}'
    end.to_s

    json_data = '{"name": "flare", "children":' + json_data + '}'

    File.open("flare.json", 'w') { |file| file.write(json_data) }
  end

  def call env
    case path
    when "/"
      @data 
      ['200',
       header,
       ['<h1> 我是首頁歡迎畫面 </h1>']
      ]
    end
  end

end
