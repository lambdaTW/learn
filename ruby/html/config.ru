# coding: utf-8
require 'net/http'
require 'open-uri'
require 'json'
require 'erb'

class Web

  def header
    {'Content-Type' => 'text/html; charset=utf-8'}
  end

  def query_key list, key
    list.each do |item|
      item = item.split("=")
      ikey = item.first
      ival = item.last
      if key == ikey
        return ival
      end
    end
    return ""
  end    

  def call env
    path = env["PATH_INFO"]
    case path
    when "/"
      ['200',
       header,
       ['<h1> 我是首頁歡迎畫面 </h1>']
      ]
    when "/about"
      ['200',
       header,
       ['<h1> 自我介紹 </h1></br><h2>I am lambda</h2>']
      ]
    when "/say"
      something = query_key env["QUERY_STRING"].split("&"), "message"
      ['200', header, [ '你說了： ' + something ]]
    when "/map"
      @locations = get_ubike
      content = ERB.new(File.read("ubike.html"))
      ['200', header, [content.result(binding)]]
    else
      ['404',
       header,
       [env["PATH_INFO"].to_s + env["QUERY_STRING"].split("=").last]
      ]
    end
  end

  private
  def get_ubike
    url = URI("http://data.ntpc.gov.tw/api/v1/rest/datastore/382000000A-000352-001")
    raw_data = Net::HTTP.get(url)

    JSON.parse(raw_data)["result"]["records"].map do |raw|
      [raw["sna"],raw["lat"].to_f,raw["lng"].to_f]
    end.to_s
  end
  
end

run Web.new
