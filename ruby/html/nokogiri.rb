require 'nokogiri'
require 'open-uri'
require 'json'

# url="http://materializecss.com/table.html"

# raw_data = Nokogiri::HTML(open(url))

# raw_data.css("#nav-mobile li ul li").each do |raw|
#   puts raw.css("a").first.text
# end

url = "http://data.ntpc.gov.tw/api/v1/rest/datastore/382000000A-000352-001"
raw_data = Nokogiri::HTML(open(url))

location = JSON.parse(raw_data)["result"]["records"].map do |raw|
  "[#{raw["sna"]},#{raw["lat"].to_f},#{raw["lng"].to_f}]"
end

p location
