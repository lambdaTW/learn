require 'sinatra'

get '/' do
  @names = %w{Eddie Ryudo Jodeci}
  erb :names
end

get '/hello/:name' do
  @name = params[:name]
  erb :index
end

post '/hello' do
  "Hello #{params['name']}!"
end
