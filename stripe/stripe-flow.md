# Scenarios flow
## Always success pay and unsubscription
```
customer.subscription.created
invoice.payment_succeeded
invoice.payment_succeeded
invoice.payment_succeeded
invoice.payment_succeeded
invoice.payment_succeeded
customer.subscription.deleted
```
## Fail to unpaid be cancelled
```
customer.subscription.created
invoice.payment_succeeded
invoice.payment_failed
invoice.payment_failed
invoice.payment_failed
customer.subscription.deleted
```
## Refund
```
customer.subscription.created
invoice.payment_succeeded
charge.refunded
customer.subscription.deleted
```

# Webhook data
## Subscription
- customer.subscription.created
    - Event.data.object.customer
        - type: Event.type
        - price: Event.data.object.plan.amount / 100.0
        - date: Event.data.object.created
        - status: 'subscription'
        - click link: None
        - subscription_id: Event.data.object.id
- customer.subscription.deleted
    - Event.data.object.customer
        - type: Event.type
        - price: Event.data.object.plan.amount / 100.0
        - date: Event.data.object.canceled_at
        - status: 'canceled'
        - click link: None
        - subscription_id: Event.data.object.id
## Invoice
- invoice.payment_succeeded
    - Event.data.object.customer
        - type: Event.type
        - price: Event.data.object.amount_paid / 100.0
        - date: Event.data.object.date
        - status: 'renewer'
        - click link: Event.data.object.invoice_pdf
        - subscription_id: Event.data.object.subscription
- invoice.payment_failed
    - Event.data.object.customer
        - type: Event.type
        - price: Event.data.object.amount_due / 100.0
        - date: Event.data.object.date
        - status: 'fail'
        - click link: Event.data.object.invoice_pdf
        - subscription_id: Event.data.object.subscription
## Refund
- charge.refunded
    - Event.data.object.customer
        - type: Event.type
        - price: Event.data.object.amount / 100.0
        - date: Event.data.object.created
        - status: 'refunded'
        - click link: stripe.Invoices.retrieve(Event.data.object.invoice).invoice_pdf
        - subscription_id: None